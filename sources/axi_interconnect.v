////--------------------------------------------------------------////
// File 		 : axi_interconnect.v
// Author		 : Andrew Varghese
// Version       : 0.0
// Date          : 19th April 2014
// Description	 : This module will instantiate all the Processor, Memory and Slave modules.
//                 This will be the top module for synthesis if axi master and slave devices
//                 are replaced by actual processors.
// Updated by Yiheng 04/2015
////--------------------------------------------------------------////


module	axi_interconnect (
		 input ACLK,
		 input ARESETn,  

         //////////////////////////////////////////////////////////////////////////
         //Processor0 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
     output					PROC0_AWREADY,//Write address ready
		 input	[2:0]		PROC0_AWID, // Write address ID //decide on the ID
		 input	[31:0] 	PROC0_AWADDR, //Write address //decide on the address width
		 input 					PROC0_AWVALID,//Write address ready
		//Write data channel
		 output 				PROC0_WREADY,//Write ready
		 input 	[2:0] 	PROC0_WID,//Write ID tag
		 input 	[127:0]	PROC0_WDATA,//Write data
		 input 					PROC0_WVALID,//Write valid
		//write response channel
		 output	[2:0] 	PROC0_BID,//Response ID tag
		 output [1:0] 	PROC0_BRESP,//Write response
		 output 				PROC0_BVALID,//Write response valid
		 input 					PROC0_BREADY,//Response ready
		//read address channel
		 output 				PROC0_ARREADY,//Write address ready
		 input	[2:0] 	PROC0_ARID, // Read address ID //decide on the ID
		 input 	[31:0] 	PROC0_ARADDR, //Read address //decide on the address width
		 input 					PROC0_ARVALID,//Read address ready
		//read data channel
		 output [2:0] 	PROC0_RID,//Read ID tag
		 output [127:0] PROC0_RDATA,//Read data
		 output 				PROC0_RVALID,//Read valid
		 input 					PROC0_RREADY,//Read ready
		 
         //////////////////////////////////////////////////////////////////////////
         //Processor1 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
     output					PROC1_AWREADY,//Write address ready
		 input	[2:0]		PROC1_AWID, // Write address ID //decide on the ID
		 input	[31:0] 	PROC1_AWADDR, //Write address //decide on the address width
		 input 					PROC1_AWVALID,//Write address ready
		//Write data channel
		 output 				PROC1_WREADY,//Write ready
		 input 	[2:0] 	PROC1_WID,//Write ID tag
		 input 	[127:0]	PROC1_WDATA,//Write data
		 input 					PROC1_WVALID,//Write valid
		//write response channel
		 output	[2:0] 	PROC1_BID,//Response ID tag
		 output [1:0] 	PROC1_BRESP,//Write response
		 output 				PROC1_BVALID,//Write response valid
		 input 					PROC1_BREADY,//Response ready
		//read address channel
		 output 				PROC1_ARREADY,//Write address ready
		 input	[2:0] 	PROC1_ARID, // Read address ID //decide on the ID
		 input 	[31:0] 	PROC1_ARADDR, //Read address //decide on the address width
		 input 					PROC1_ARVALID,//Read address ready
		//read data channel
		 output [2:0] 	PROC1_RID,//Read ID tag
		 output [127:0] PROC1_RDATA,//Read data
		 output 				PROC1_RVALID,//Read valid
		 input 					PROC1_RREADY,//Read ready		 
         
         //////////////////////////////////////////////////////////////////////////
         //Processor2 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
     output					PROC2_AWREADY,//Write address ready
		 input	[2:0]		PROC2_AWID, // Write address ID //decide on the ID
		 input	[31:0] 	PROC2_AWADDR, //Write address //decide on the address width
		 input 					PROC2_AWVALID,//Write address ready
		//Write data channel
		 output 				PROC2_WREADY,//Write ready
		 input 	[2:0] 	PROC2_WID,//Write ID tag
		 input 	[127:0]	PROC2_WDATA,//Write data
		 input 					PROC2_WVALID,//Write valid
		//write response channel
		 output	[2:0] 	PROC2_BID,//Response ID tag
		 output [1:0] 	PROC2_BRESP,//Write response
		 output 				PROC2_BVALID,//Write response valid
		 input 					PROC2_BREADY,//Response ready
		//read address channel
		 output 				PROC2_ARREADY,//Write address ready
		 input	[2:0] 	PROC2_ARID, // Read address ID //decide on the ID
		 input 	[31:0] 	PROC2_ARADDR, //Read address //decide on the address width
		 input 					PROC2_ARVALID,//Read address ready
		//read data channel
		 output [2:0] 	PROC2_RID,//Read ID tag
		 output [127:0] PROC2_RDATA,//Read data
		 output 				PROC2_RVALID,//Read valid
		 input 					PROC2_RREADY,//Read ready
		 
         //////////////////////////////////////////////////////////////////////////
         //Processor3 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
     output					PROC3_AWREADY,//Write address ready
		 input	[2:0]		PROC3_AWID, // Write address ID //decide on the ID
		 input	[31:0] 	PROC3_AWADDR, //Write address //decide on the address width
		 input 					PROC3_AWVALID,//Write address ready
		//Write data channel
		 output 				PROC3_WREADY,//Write ready
		 input 	[2:0] 	PROC3_WID,//Write ID tag
		 input 	[127:0]	PROC3_WDATA,//Write data
		 input 					PROC3_WVALID,//Write valid
		//write response channel
		 output	[2:0] 	PROC3_BID,//Response ID tag
		 output [1:0] 	PROC3_BRESP,//Write response
		 output 				PROC3_BVALID,//Write response valid
		 input 					PROC3_BREADY,//Response ready
		//read address channel
		 output 				PROC3_ARREADY,//Write address ready
		 input	[2:0] 	PROC3_ARID, // Read address ID //decide on the ID
		 input 	[31:0] 	PROC3_ARADDR, //Read address //decide on the address width
		 input 					PROC3_ARVALID,//Read address ready
		//read data channel
		 output [2:0] 	PROC3_RID,//Read ID tag
		 output [127:0] PROC3_RDATA,//Read data
		 output 				PROC3_RVALID,//Read valid
		 input 					PROC3_RREADY,//Read ready
	
				//////////////////////////////////////////////////////////////////////////
        //Slave4 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		input 					SLAVE4_AWREADY,//Write address ready
		output	[2:0] 	SLAVE4_AWID, // Write address ID //decide on the ID
		output 	[31:0]	SLAVE4_AWADDR, //Write address //decide on the address width
		output 					SLAVE4_AWVALID,//Write address ready
		//Write data channel
		input 					SLAVE4_WREADY,//Write ready
		output 	[2:0] 	SLAVE4_WID,//Write ID tag
		output	[127:0]	SLAVE4_WDATA,//Write data
		output 					SLAVE4_WVALID,//Write valid
		//write response channel
		input 	[2:0] 	SLAVE4_BID,//Response ID tag
		input 	[1:0] 	SLAVE4_BRESP,//Write response
		input 					SLAVE4_BVALID,//Write response valid
		output 					SLAVE4_BREADY,//Response ready
		//read address channel
		input 					SLAVE4_ARREADY,//Write address ready
		output 	[2:0] 	SLAVE4_ARID, // Read address ID //decide on the ID
		output 	[31:0] 	SLAVE4_ARADDR, //Read address //decide on the address width
		output 					SLAVE4_ARVALID,//Read address ready
		//read data channel
		input 	[2:0] 	SLAVE4_RID,//Read ID tag
		input 	[127:0] SLAVE4_RDATA,//Read data
		input 					SLAVE4_RVALID,//Read valid
		output 					SLAVE4_RREADY,//Read ready		

				//////////////////////////////////////////////////////////////////////////
        //Slave5 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		input 					SLAVE5_AWREADY,//Write address ready
		output	[2:0] 	SLAVE5_AWID, // Write address ID //decide on the ID
		output 	[31:0]	SLAVE5_AWADDR, //Write address //decide on the address width
		output 					SLAVE5_AWVALID,//Write address ready
		//Write data channel
		input 					SLAVE5_WREADY,//Write ready
		output 	[2:0] 	SLAVE5_WID,//Write ID tag
		output	[127:0]	SLAVE5_WDATA,//Write data
		output 					SLAVE5_WVALID,//Write valid
		//write response channel
		input 	[2:0] 	SLAVE5_BID,//Response ID tag
		input 	[1:0] 	SLAVE5_BRESP,//Write response
		input 					SLAVE5_BVALID,//Write response valid
		output 					SLAVE5_BREADY,//Response ready
		//read address channel
		input 					SLAVE5_ARREADY,//Write address ready
		output 	[2:0] 	SLAVE5_ARID, // Read address ID //decide on the ID
		output 	[31:0] 	SLAVE5_ARADDR, //Read address //decide on the address width
		output 					SLAVE5_ARVALID,//Read address ready
		//read data channel
		input 	[2:0] 	SLAVE5_RID,//Read ID tag
		input 	[127:0] SLAVE5_RDATA,//Read data
		input 					SLAVE5_RVALID,//Read valid
		output 					SLAVE5_RREADY,//Read ready		
		
				//////////////////////////////////////////////////////////////////////////
        //Slave6 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		input 					SLAVE6_AWREADY,//Write address ready
		output	[2:0] 	SLAVE6_AWID, // Write address ID //decide on the ID
		output 	[31:0]	SLAVE6_AWADDR, //Write address //decide on the address width
		output 					SLAVE6_AWVALID,//Write address ready
		//Write data channel
		input 					SLAVE6_WREADY,//Write ready
		output 	[2:0] 	SLAVE6_WID,//Write ID tag
		output	[127:0]	SLAVE6_WDATA,//Write data
		output 					SLAVE6_WVALID,//Write valid
		//write response channel
		input 	[2:0] 	SLAVE6_BID,//Response ID tag
		input 	[1:0] 	SLAVE6_BRESP,//Write response
		input 					SLAVE6_BVALID,//Write response valid
		output 					SLAVE6_BREADY,//Response ready
		//read address channel
		input 					SLAVE6_ARREADY,//Write address ready
		output 	[2:0] 	SLAVE6_ARID, // Read address ID //decide on the ID
		output 	[31:0] 	SLAVE6_ARADDR, //Read address //decide on the address width
		output 					SLAVE6_ARVALID,//Read address ready
		//read data channel
		input 	[2:0] 	SLAVE6_RID,//Read ID tag
		input 	[127:0] SLAVE6_RDATA,//Read data
		input 					SLAVE6_RVALID,//Read valid
		output 					SLAVE6_RREADY,//Read ready
		
				//////////////////////////////////////////////////////////////////////////
        //Slave7 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		input 					SLAVE7_AWREADY,//Write address ready
		output	[2:0] 	SLAVE7_AWID, // Write address ID //decide on the ID
		output 	[31:0]	SLAVE7_AWADDR, //Write address //decide on the address width
		output 					SLAVE7_AWVALID,//Write address ready
		//Write data channel
		input 					SLAVE7_WREADY,//Write ready
		output 	[2:0] 	SLAVE7_WID,//Write ID tag
		output	[127:0]	SLAVE7_WDATA,//Write data
		output 					SLAVE7_WVALID,//Write valid
		//write response channel
		input 	[2:0] 	SLAVE7_BID,//Response ID tag
		input 	[1:0] 	SLAVE7_BRESP,//Write response
		input 					SLAVE7_BVALID,//Write response valid
		output 					SLAVE7_BREADY,//Response ready
		//read address channel
		input 					SLAVE7_ARREADY,//Write address ready
		output 	[2:0] 	SLAVE7_ARID, // Read address ID //decide on the ID
		output 	[31:0] 	SLAVE7_ARADDR, //Read address //decide on the address width
		output 					SLAVE7_ARVALID,//Read address ready
		//read data channel
		input 	[2:0] 	SLAVE7_RID,//Read ID tag
		input 	[127:0] SLAVE7_RDATA,//Read data
		input 					SLAVE7_RVALID,//Read valid
		output 					SLAVE7_RREADY//Read ready
		

         
	);

wire p0_valid,p1_valid,p2_valid,p3_valid;
wire network_p0_ready, network_p1_ready, network_p2_ready, network_p3_ready, network_m4_ready, network_m5_ready, network_m6_ready, network_m7_ready;
wire [159:0] p0_data_out, p1_data_out, p2_data_out, p3_data_out, m4_data_out, m5_data_out, m6_data_out, m7_data_out;
wire network_p0_valid, network_p1_valid, network_p2_valid, network_p3_valid, network_m4_valid, network_m5_valid, network_m6_valid, network_m7_valid;
wire [159:0] network_p0_data_out, network_p1_data_out, network_p2_data_out, network_p3_data_out, network_m4_data_out, network_m5_data_out, network_m6_data_out, network_m7_data_out;
wire p0_ready,p1_ready,p2_ready,p3_ready;


wire MIF4_valid,MIF5_valid,MIF6_valid,MIF7_valid;
wire MIF4_ready,MIF5_ready,MIF6_ready,MIF7_ready;
   



//Slave interface for the AXI Master (Processor / L1 cache)
SI  #(3'b000)  SI0  (	
				.ACLK					(ACLK), 
				.ARESETn			(ARESETn), 		//Global signals
				.AWID					(PROC0_AWID), 
				.AWADDR				(PROC0_AWADDR), 
				.AWVALID			(PROC0_AWVALID), 
				.AWREADY			(PROC0_AWREADY),	//Write address signals
				.WID					(PROC0_WID),
				.WDATA				(PROC0_WDATA), 
				.WVALID				(PROC0_WVALID),
				.WREADY				(PROC0_WREADY), //Write data signals
				.BID					(PROC0_BID),
				.BRESP				(PROC0_BRESP), 
				.BVALID				(PROC0_BVALID), 
				.BREADY				(PROC0_BREADY),	//Write response signals
				.ARID					(PROC0_ARID), 
				.ARADDR				(PROC0_ARADDR), 
				.ARVALID			(PROC0_ARVALID), 
				.ARREADY			(PROC0_ARREADY),		//Read address signals
				.RID					(PROC0_RID), 
				.RDATA				(PROC0_RDATA), 
				.RVALID				(PROC0_RVALID), 
				.RREADY				(PROC0_RREADY),
                //signals to router
				.M2R_data_out					(p0_data_out), //o/p
				.IF_valid							(p0_valid), //o/p 
				.router_ready					(network_p0_ready), //i/p
                //signals from router
				.R2M_data_input				(network_p0_data_out), //i/p
				.router_valid					(network_p0_valid), //i/p
				.IF_ready							(p0_ready) //o/p
			);

SI  #(3'b001)  SI1  (	
				.ACLK					(ACLK), 
				.ARESETn			(ARESETn), 		//Global signals
				.AWID					(PROC1_AWID), 
				.AWADDR				(PROC1_AWADDR), 
				.AWVALID			(PROC1_AWVALID), 
				.AWREADY			(PROC1_AWREADY),	//Write address signals
				.WID					(PROC1_WID),
				.WDATA				(PROC1_WDATA), 
				.WVALID				(PROC1_WVALID),
				.WREADY				(PROC1_WREADY), //Write data signals
				.BID					(PROC1_BID),
				.BRESP				(PROC1_BRESP), 
				.BVALID				(PROC1_BVALID), 
				.BREADY				(PROC1_BREADY),	//Write response signals
				.ARID					(PROC1_ARID), 
				.ARADDR				(PROC1_ARADDR), 
				.ARVALID			(PROC1_ARVALID), 
				.ARREADY			(PROC1_ARREADY),		//Read address signals
				.RID					(PROC1_RID), 
				.RDATA				(PROC1_RDATA), 
				.RVALID				(PROC1_RVALID), 
				.RREADY				(PROC1_RREADY),
                //signals to router
				.M2R_data_out					(p1_data_out), //o/p
				.IF_valid							(p1_valid), //o/p 
				.router_ready					(network_p1_ready), //i/p
                //signals from router
				.R2M_data_input				(network_p1_data_out), //i/p
				.router_valid					(network_p1_valid), //i/p
				.IF_ready							(p1_ready) //o/p
			);			
			
SI  #(3'b010)  SI2  (	
				.ACLK					(ACLK), 
				.ARESETn			(ARESETn), 		//Global signals
				.AWID					(PROC2_AWID), 
				.AWADDR				(PROC2_AWADDR), 
				.AWVALID			(PROC2_AWVALID), 
				.AWREADY			(PROC2_AWREADY),	//Write address signals
				.WID					(PROC2_WID),
				.WDATA				(PROC2_WDATA), 
				.WVALID				(PROC2_WVALID),
				.WREADY				(PROC2_WREADY), //Write data signals
				.BID					(PROC2_BID),
				.BRESP				(PROC2_BRESP), 
				.BVALID				(PROC2_BVALID), 
				.BREADY				(PROC2_BREADY),	//Write response signals
				.ARID					(PROC2_ARID), 
				.ARADDR				(PROC2_ARADDR), 
				.ARVALID			(PROC2_ARVALID), 
				.ARREADY			(PROC2_ARREADY),		//Read address signals
				.RID					(PROC2_RID), 
				.RDATA				(PROC2_RDATA), 
				.RVALID				(PROC2_RVALID), 
				.RREADY				(PROC2_RREADY),
                 //signals to router
				.M2R_data_out		(p2_data_out), //o/p
				.IF_valid				(p2_valid), //o/p 
				.router_ready		(network_p2_ready), //i/p
                //signals from router
				.R2M_data_input	(network_p2_data_out), //i/p
				.router_valid		(network_p2_valid), //i/p
				.IF_ready				(p2_ready) //o/p
			);

SI  #(3'b011)  SI3  (	
				.ACLK					(ACLK), 
				.ARESETn			(ARESETn), 		//Global signals
				.AWID					(PROC3_AWID), 
				.AWADDR				(PROC3_AWADDR), 
				.AWVALID			(PROC3_AWVALID), 
				.AWREADY			(PROC3_AWREADY),	//Write address signals
				.WID					(PROC3_WID),
				.WDATA				(PROC3_WDATA), 
				.WVALID				(PROC3_WVALID),
				.WREADY				(PROC3_WREADY), //Write data signals
				.BID					(PROC3_BID),
				.BRESP				(PROC3_BRESP), 
				.BVALID				(PROC3_BVALID), 
				.BREADY				(PROC3_BREADY),	//Write response signals
				.ARID					(PROC3_ARID), 
				.ARADDR				(PROC3_ARADDR), 
				.ARVALID			(PROC3_ARVALID), 
				.ARREADY			(PROC3_ARREADY),		//Read address signals
				.RID					(PROC3_RID), 
				.RDATA				(PROC3_RDATA), 
				.RVALID				(PROC3_RVALID), 
				.RREADY				(PROC3_RREADY),
                //signals to router
				.M2R_data_out		(p3_data_out), //o/p
				.IF_valid				(p3_valid), //o/p 
				.router_ready		(network_p3_ready), //i/p
                //signals from router
				.R2M_data_input	(network_p3_data_out), //i/p
				.router_valid		(network_p3_valid), //i/p
				.IF_ready				(p3_ready) //o/p
			);			

//Master interface for the AXI Slave (Memory)
master_interface #(.SLAVEID(3'b100)) MI4 (
			.ACLK			(ACLK), 
			.ARESETn	(ARESETn), 		//Global signals
			.AWID			(SLAVE4_AWID),
			.AWADDR		(SLAVE4_AWADDR), 
			.AWVALID	(SLAVE4_AWVALID), 
			.AWREADY	(SLAVE4_AWREADY),	//Write address signals
			.WID			(SLAVE4_WID), 
			.WDATA		(SLAVE4_WDATA), 
			.WVALID		(SLAVE4_WVALID), 
			.WREADY		(SLAVE4_WREADY), //Write data signals
			.BID			(SLAVE4_BID), 
			.BRESP		(SLAVE4_BRESP), 
			.BVALID		(SLAVE4_BVALID), 
			.BREADY		(SLAVE4_BREADY),	//Write response signals
			.ARID			(SLAVE4_ARID), 
			.ARADDR		(SLAVE4_ARADDR), 
			.ARVALID	(SLAVE4_ARVALID), 
			.ARREADY	(SLAVE4_ARREADY),		//Read address signals
			.RID			(SLAVE4_RID), 
			.RDATA		(SLAVE4_RDATA), 
			.RVALID		(SLAVE4_RVALID), 
			.RREADY		(SLAVE4_RREADY),
            //Signals to Router
			.R2S_data_in		(network_m4_data_out), //i/p    --- Router_to_Slave_fifo
			.router_valid		(network_m4_valid), //i/p   ---- Router_to_Slave_fifo
			.IF_ready				(MIF4_ready), //o/p  ---- Router_to_Slave_fifo
            //Signals from Router
			.S2R_data_out		(m4_data_out), //o/p   ---- S_T_R_FIFO
			.router_ready		(network_m4_ready), //i/p   ---- S_T_R_FIFO
			// guo: bug fixed
			.IF_valid				(MIF4_valid) //o/p    ---- S_T_R_FIFO
			);			
//Master interface for the AXI Slave (Memory)
master_interface #(.SLAVEID(3'b101)) MI5 (
			.ACLK			(ACLK), 
			.ARESETn	(ARESETn), 		//Global signals
			.AWID			(SLAVE5_AWID),
			.AWADDR		(SLAVE5_AWADDR), 
			.AWVALID	(SLAVE5_AWVALID), 
			.AWREADY	(SLAVE5_AWREADY),	//Write address signals
			.WID			(SLAVE5_WID), 
			.WDATA		(SLAVE5_WDATA), 
			.WVALID		(SLAVE5_WVALID), 
			.WREADY		(SLAVE5_WREADY), //Write data signals
			.BID			(SLAVE5_BID), 
			.BRESP		(SLAVE5_BRESP), 
			.BVALID		(SLAVE5_BVALID), 
			.BREADY		(SLAVE5_BREADY),	//Write response signals
			.ARID			(SLAVE5_ARID), 
			.ARADDR		(SLAVE5_ARADDR), 
			.ARVALID	(SLAVE5_ARVALID), 
			.ARREADY	(SLAVE5_ARREADY),		//Read address signals
			.RID			(SLAVE5_RID), 
			.RDATA		(SLAVE5_RDATA), 
			.RVALID		(SLAVE5_RVALID), 
			.RREADY		(SLAVE5_RREADY),
            //Signals to Router
			.R2S_data_in		(network_m5_data_out), //i/p    --- Router_to_Slave_fifo
			.router_valid		(network_m5_valid), //i/p   ---- Router_to_Slave_fifo
			.IF_ready				(MIF5_ready), //o/p  ---- Router_to_Slave_fifo
            //Signals from Router
			.S2R_data_out		(m5_data_out), //o/p   ---- S_T_R_FIFO
			.router_ready		(network_m5_ready), //i/p   ---- S_T_R_FIFO
			// guo: bug fixed
			.IF_valid				(MIF5_valid) //o/p    ---- S_T_R_FIFO
			);
			
//Master interface for the AXI Slave (Memory)
master_interface #(.SLAVEID(3'b110)) MI6 (
			.ACLK			(ACLK), 
			.ARESETn	(ARESETn), 		//Global signals
			.AWID			(SLAVE6_AWID),
			.AWADDR		(SLAVE6_AWADDR), 
			.AWVALID	(SLAVE6_AWVALID), 
			.AWREADY	(SLAVE6_AWREADY),	//Write address signals
			.WID			(SLAVE6_WID), 
			.WDATA		(SLAVE6_WDATA), 
			.WVALID		(SLAVE6_WVALID), 
			.WREADY		(SLAVE6_WREADY), //Write data signals
			.BID			(SLAVE6_BID), 
			.BRESP		(SLAVE6_BRESP), 
			.BVALID		(SLAVE6_BVALID), 
			.BREADY		(SLAVE6_BREADY),	//Write response signals
			.ARID			(SLAVE6_ARID), 
			.ARADDR		(SLAVE6_ARADDR), 
			.ARVALID	(SLAVE6_ARVALID), 
			.ARREADY	(SLAVE6_ARREADY),		//Read address signals
			.RID			(SLAVE6_RID), 
			.RDATA		(SLAVE6_RDATA), 
			.RVALID		(SLAVE6_RVALID), 
			.RREADY		(SLAVE6_RREADY),
            //Signals to Router
			.R2S_data_in		(network_m6_data_out), //i/p    --- Router_to_Slave_fifo
			.router_valid		(network_m6_valid), //i/p   ---- Router_to_Slave_fifo
			.IF_ready				(MIF6_ready), //o/p  ---- Router_to_Slave_fifo
            //Signals from Router
			.S2R_data_out		(m6_data_out), //o/p   ---- S_T_R_FIFO
			.router_ready		(network_m6_ready), //i/p   ---- S_T_R_FIFO
			// guo: bug fixed
			.IF_valid				(MIF6_valid) //o/p    ---- S_T_R_FIFO
			);

//Master interface for the AXI Slave (Memory)
master_interface #(.SLAVEID(3'b111)) MI7 (
			.ACLK			(ACLK), 
			.ARESETn	(ARESETn), 		//Global signals
			.AWID			(SLAVE7_AWID),
			.AWADDR		(SLAVE7_AWADDR), 
			.AWVALID	(SLAVE7_AWVALID), 
			.AWREADY	(SLAVE7_AWREADY),	//Write address signals
			.WID			(SLAVE7_WID), 
			.WDATA		(SLAVE7_WDATA), 
			.WVALID		(SLAVE7_WVALID), 
			.WREADY		(SLAVE7_WREADY), //Write data signals
			.BID			(SLAVE7_BID), 
			.BRESP		(SLAVE7_BRESP), 
			.BVALID		(SLAVE7_BVALID), 
			.BREADY		(SLAVE7_BREADY),	//Write response signals
			.ARID			(SLAVE7_ARID), 
			.ARADDR		(SLAVE7_ARADDR), 
			.ARVALID	(SLAVE7_ARVALID), 
			.ARREADY	(SLAVE7_ARREADY),		//Read address signals
			.RID			(SLAVE7_RID), 
			.RDATA		(SLAVE7_RDATA), 
			.RVALID		(SLAVE7_RVALID), 
			.RREADY		(SLAVE7_RREADY),
            //Signals to Router
			.R2S_data_in		(network_m7_data_out), //i/p    --- Router_to_Slave_fifo
			.router_valid		(network_m7_valid), //i/p   ---- Router_to_Slave_fifo
			.IF_ready				(MIF7_ready), //o/p  ---- Router_to_Slave_fifo
            //Signals from Router
			.S2R_data_out		(m7_data_out), //o/p   ---- S_T_R_FIFO
			.router_ready		(network_m7_ready), //i/p   ---- S_T_R_FIFO
			// guo: bug fixed
			.IF_valid				(MIF7_valid) //o/p    ---- S_T_R_FIFO
			);
			

   
network nwk(
					.clk							(ACLK),
					.reset						(ARESETn),
            // Signals for dout to Master/Slave Interface
					.r0_writevalid		(p0_valid), 
					.r1_writevalid		(p1_valid), 
					.r2_writevalid		(p2_valid),
					.r3_writevalid		(p3_valid), 
					.r4_writevalid		(MIF4_valid), 
					.r5_writevalid		(MIF5_valid), 
					.r6_writevalid		(MIF6_valid), 
					.r7_writevalid		(MIF7_valid),
					.r0_in_din5				(p0_data_out),
					.r1_in_din5				(p1_data_out),
					.r2_in_din5				(p2_data_out),
					.r3_in_din5				(p3_data_out),
					.r4_in_din5				(m4_data_out),
					.r5_in_din5				(m5_data_out),
					.r6_in_din5				(m6_data_out),
					.r7_in_din5				(m7_data_out),
					.r0_in_ready5			(network_p0_ready),
					.r1_in_ready5			(network_p1_ready),
					.r2_in_ready5			(network_p2_ready),
					.r3_in_ready5			(network_p3_ready),
					.r4_in_ready5			(network_m4_ready),
					.r5_in_ready5			(network_m5_ready),
					.r6_in_ready5			(network_m6_ready),
					.r7_in_ready5			(network_m7_ready),
            // Signals for din from Master/Slave Interface
					.r0_readvalid			(p0_ready),
					.r1_readvalid			(p1_ready),
					.r2_readvalid			(p2_ready),
					.r3_readvalid			(p3_ready),
					.r4_readvalid			(MIF4_ready), 
					.r5_readvalid			(MIF5_ready),
					.r6_readvalid			(MIF6_ready),
					.r7_readvalid			(MIF7_ready),
					
					.r0_out_dout5			(network_p0_data_out),
					.r1_out_dout5			(network_p1_data_out),
					.r2_out_dout5			(network_p2_data_out),
          .r3_out_dout5			(network_p3_data_out),
          .r4_out_dout5			(network_m4_data_out),
          .r5_out_dout5			(network_m5_data_out),
          .r6_out_dout5			(network_m6_data_out),
          .r7_out_dout5			(network_m7_data_out),
          .r0_out_write			(network_p0_valid),
          .r1_out_write			(network_p1_valid),
          .r2_out_write			(network_p2_valid),
          .r3_out_write			(network_p3_valid),
          .r4_out_write			(network_m4_valid),
          .r5_out_write			(network_m5_valid),
          .r6_out_write			(network_m6_valid),
          .r7_out_write			(network_m7_valid)
                    
					);


endmodule
	
	
	
	
	
	
	
	
	
	
