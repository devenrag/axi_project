////--------------------------------------------------------------
// File 		 : PE_FIFO1.v
// Author		 : Hari Prasanth Govindaraju
// Version       : 0.0
// Date          : 23th April 2014
// Description	 : This FIFO module is the PE FIFO to be used in the Router design. 
// 				         It is a 2 location 160 bit wide FIFO capable of storing 2 packets.
//				         The writer views it as 2 location FIFO each 160 bits wide (160*2 FIFO)
//				         The reader views it as 10 location FIFO each 32 bits wide (32*10 FIFO)
// Updated by Yiheng 04/2015
////--------------------------------------------------------------

module PE_FIFO1 (clk, reset, put, get, data_in, empty_bar, full_bar, data_out);
input data_in, clk;
input get, put, reset;
output data_out, empty_bar, full_bar;

reg [159:0]FIFO[0:1];
reg [31:0]data_out;
reg wen, ren; //index;
reg [4:0] wr_ptr;
reg [4:0] rd_ptr;
reg empty_bar, full_bar;
wire [159:0] data_in;

// just for debugging
wire full, empty;
assign full = ~full_bar;
assign empty = ~empty_bar;

always@(*)
begin

full_bar <= 1'b0;
empty_bar <= 1'b0;
wen <= 1'b0;
ren <= 1'b0;

if((wr_ptr[4:3]-rd_ptr[4:3])!=2'b10)
	begin
	full_bar <= 1;
	if(put==1'b1)
		begin
		wen <= 1;
		end
	end

if(wr_ptr[4:3]!=rd_ptr[4:3])
	begin
	empty_bar <= 1;
	if(get==1'b1)
		begin
		ren <= 1;
		end
	end

if(rd_ptr[2:0] == 3'b000 ) 
	begin
	data_out[31:0] <= FIFO[rd_ptr[3]][31:0];
	end

if(rd_ptr[2:0] == 3'b001 ) 
	begin
	data_out[31:0] <= FIFO[rd_ptr[3]][63:32];
	end

if(rd_ptr[2:0] == 3'b010 ) 
	begin
	data_out[31:0] <= FIFO[rd_ptr[3]][95:64];
	end

if(rd_ptr[2:0] == 3'b011 ) 
	begin
	data_out[31:0] <= FIFO[rd_ptr[3]][127:96];
	end

if(rd_ptr[2:0] == 3'b100 ) 
	begin
	data_out[31:0] <= FIFO[rd_ptr[3]][159:128];
	end

end

always @(posedge clk)
begin
if(reset == 1'b0)
	begin
	wr_ptr <= 5'b0;
	rd_ptr <= 5'b0;
	end
else
	begin
	if(wen == 1'b1)
		begin
	  FIFO[wr_ptr[3]] <= data_in;
    wr_ptr[4:3] <= wr_ptr[4:3] + 1;
		end
	
	if(ren == 1'b1)
		begin
	  if(rd_ptr[2:0] != 3'b100) 
			begin
			rd_ptr <= rd_ptr + 1;
			end
	  else
			begin
			rd_ptr[4:3] <= rd_ptr[4:3]+1;
			rd_ptr[2:0] <= 3'b000;
			end
	  end
	end
end
endmodule


