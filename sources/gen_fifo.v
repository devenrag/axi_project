//---------------------------------------------------------------
// Name: 		gen_fifo.v
// Author: 	Yiheng
// based on Hari's FIFO, 04/2015
// # of locations can be any integer 
// # of data width can be any integer
 

////--------------------------------------------------------------////
// File 		 : IP_OP_FIFO.v
// Author		 : Hari Prasanth Govindaraju
// Version       : 0.0
// Date          : 23th April 2014
// Description	 : This FIFO module is the INPUT/OUTPUT FIFO to be used in the Router design. 
// 				   It is a 10 location 32 bit wide FIFO capable of storing 2 packets (10 phits)
////--------------------------------------------------------------////

module fifo (clk, resetn, put, get, data_in, empty, empty_bar, full, full_bar, one_location, data_out);

parameter DEPTH = 10; 
parameter DATA_WIDTH = 32;

parameter POINTER_WIDTH = clog2(DEPTH);


// function to calculate pointer width
// for example, depth=10 will return 4; depth=8 will return 3
function integer clog2;
input integer value;
begin
    value = value - 1;
    for (clog2=0;value>0;clog2=clog2+1)
        value = value >> 1;
end
endfunction

input data_in, clk;
input get, put, resetn;
output data_out, empty, empty_bar, full, full_bar, one_location;

reg [(DATA_WIDTH-1):0]mem[(DEPTH-1):0];
wire [(DATA_WIDTH-1):0]data_out;
//reg [3:0] fill_count;
reg wen, ren;
reg [POINTER_WIDTH:0] wr_ptr,rd_ptr;
reg empty_bar, full_bar, one_location;
wire [(DATA_WIDTH-1):0] data_in;
wire full, empty;

assign data_out = mem[rd_ptr[(POINTER_WIDTH-1):0]];
assign full = ~full_bar;
assign empty = ~empty_bar;

always @(*)
begin
wen <= 1'b0;
ren <= 1'b0;
full_bar <= 1'b0;
empty_bar <= 1'b0;
one_location <= 0;

if((wr_ptr-rd_ptr)!={1'b1,{(POINTER_WIDTH){1'b0}}})
	begin
	full_bar <= 1;
	if(put==1)
		begin
		wen <= 1;
		end
	end

if(wr_ptr!=rd_ptr)
	begin
	empty_bar <= 1;
	if(get == 1'b1)
		begin
		ren <= 1;
		end
	end
	// one_location: fifo has one empty location left, this output is used in slave interface
if((wr_ptr-rd_ptr)=={1'b0,{(POINTER_WIDTH){1'b1}}})
	begin
	one_location <= 1;
	end
end


always @(posedge clk)
begin
if(resetn == 1'b0)
	begin
	wr_ptr <= 0;
	rd_ptr <= 0;
	end
else
	begin
	if(wen == 1'b1)
		begin
		mem[wr_ptr[(POINTER_WIDTH-1):0]] <= data_in;
		// the pointer updation may seem strange here, because we should consider depth is not power of 2
    wr_ptr <= wr_ptr + 1;
		if(wr_ptr[(POINTER_WIDTH-1):0] ==(DEPTH-1))
			begin
			wr_ptr[(POINTER_WIDTH-1):0] <= 0;
			wr_ptr[POINTER_WIDTH] <=  wr_ptr[POINTER_WIDTH] + 1;
			end
		end
	
	if(ren == 1'b1)
		begin
		// the pointer updation may seem strange here, because we should consider depth is not power of 2
		rd_ptr <= rd_ptr + 1;
		if (rd_ptr[(POINTER_WIDTH-1):0] == (DEPTH-1))
			begin
			rd_ptr[(POINTER_WIDTH-1):0] <= 0;
			rd_ptr[POINTER_WIDTH] <= rd_ptr[POINTER_WIDTH] + 1;
			end
		end	
	end
end
endmodule


