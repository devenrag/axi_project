////--------------------------------------------------------------////
// File 		 : tb_axi_master_proc0.v
// Author		 : Andrew Varghese
// Version       : 0.0
// Date          : 19th April 2014
// Description	 : This module will generate read and write transactions.
//                 The formate of input file proc0_test_pattern0.dat should be as follows
//                 Waitcycle, command,    Address,     Data of cache line 128bit          
//                   10           1       12345678     12345678123456781234567812345678   
//                 The above file line can be extended for Phase-2 implementation by adding AXI ID and other signals
//                 This testbench works for upto 32 bit address width. Interconnect should take care of the address mappings.
////--------------------------------------------------------------////

`timescale 1 ns / 1 ps

module axi_master_proc(
  input ACLK, //global clock
  input ARESETn, //global reset

  //write address channel
  input PROC_AWREADY,//Write address ready
  output reg [2:0] PROC_AWID, // Write address ID
  output reg [31:0] PROC_AWADDR, //Write address
  output reg PROC_AWVALID,//Write address ready
  
  //Write data channel
  input PROC_WREADY,//Write ready
  output reg [2:0] PROC_WID,//Write ID tag
  output reg [127:0] PROC_WDATA,//Write data
  output reg PROC_WVALID,//Write valid
  
  //write response channel
  input [2:0] PROC_BID,//Response ID tag
  input [1:0] PROC_BRESP,//Write response
  input PROC_BVALID,//Write response valid
  output reg PROC_BREADY,//Response ready
  
  //read address channel
  input PROC_ARREADY,//Write address ready
  output reg [2:0] PROC_ARID, // Read address ID //decide on the ID
  output reg [31:0] PROC_ARADDR, //Read address //decide on the address width
  output reg PROC_ARVALID,//Read address ready
  
  //read data channel
  input [2:0] PROC_RID,//Read ID tag
  input [127:0] PROC_RDATA,//Read data
  input PROC_RVALID,//Read valid
  output reg PROC_RREADY//Read ready
);

	`define NULL 0 
	
	parameter PID = 0;
	

		
  integer waitcycles;
  integer pending_write_responses;
  integer fin, f_dump1,f_dump;
  integer r;
  reg [2:0] Cmd;
  reg [31:0] Addr;
  reg [127:0] Data;
  reg pattern_completed;
  reg [7:0] response_wait;
  reg loop_start_debug;
  
  initial
  begin
	
	
	if(PID==0) 
		begin
		fin = $fopen("./cmds/proc0.dat", "r");
		f_dump1=$fopen("./results/proc_read0.dump", "w"); 
		//f_dump = $fopen("./results/BRESP_addr0.dump", "w");
		end
	if(PID==1)
		begin
		fin = $fopen("./cmds/proc1.dat", "r");
		f_dump1=$fopen("./results/proc_read1.dump", "w"); 
		//f_dump = $fopen("./results/BRESP_addr1.dump", "w");
		end	
	if(PID==2)
		begin
		fin = $fopen("./cmds/proc2.dat", "r");
		f_dump1=$fopen("./results/proc_read2.dump", "w"); 
		//f_dump = $fopen("./results/BRESP_addr2.dump", "w");
		end	
	if(PID==3)
		begin
		fin = $fopen("./cmds/proc3.dat", "r");
		f_dump1=$fopen("./results/proc_read3.dump", "w");
		//f_dump = $fopen("./results/BRESP_addr3.dump", "w");		
		end	
		
		
    pending_write_responses <= 0;
	pattern_completed <= 0;
	PROC_AWID <= 0; 
	PROC_AWADDR <= 0; 
	PROC_AWVALID <= 0;
	PROC_WID <= 0;
	PROC_WDATA <= 0;
	PROC_WVALID <= 0;
	PROC_BREADY <= 1;
	PROC_ARID <= 0; 
	PROC_ARADDR <= 0; 
	PROC_ARVALID <= 0;
	PROC_RREADY <= 1;
    loop_start_debug <= 0;
	response_wait <= 0;

	@(posedge ACLK);
	@(posedge ACLK);
	@(posedge ACLK);
    repeat (10) @(posedge ACLK);

	// Test Pattern File
	
    if (fin == `NULL) begin
      $display("ERROR : Could not open the file \n");
      $finish;
    end
    r = $fscanf(fin,"%d   %d    %x     %x\n", waitcycles, Cmd, Addr, Data);
    if (r == 0)	begin
	  $display("ERROR : Input file is blank\n");
	  $finish;
	end

	//Apply each test pattern starting from here
	//@(posedge ACLK);
	while (r == 4) begin
		@(posedge ACLK);
      $display("While loop executing");
      loop_start_debug <= ~ loop_start_debug;
	  if (Cmd == 0) begin
		response_wait = waitcycles;
	  end else if (Cmd == 1) begin //Read command
		PROC_ARADDR <= #0.1 Addr; 
		PROC_ARVALID <= #0.1 1'b1;
		#0.1;
		while (PROC_ARREADY == 0) begin
		  @(posedge ACLK);
		  #0.1;
		end
        @(posedge ACLK);
		PROC_ARADDR <= #0.1 0; 
		PROC_ARVALID <= #0.1 1'b0;
	  end else if (Cmd == 2) begin
		PROC_AWADDR <= #0.1 Addr; 
		PROC_AWVALID <= #0.1 1'b1;
        #0.1;
		while (PROC_AWREADY == 0) begin
		  @(posedge ACLK);
		  #0.1;
		end
        @(posedge ACLK);
		PROC_AWADDR <= #0.1 0; 
		PROC_AWVALID <= #0.1 1'b0;
		PROC_WDATA <= #0.1 Data; 
		PROC_WVALID <= #0.1 1'b1; 
		#0.1;
		while (PROC_WREADY == 0) begin
		  @(posedge ACLK);
		  #0.1;
		end
        @(posedge ACLK);
		PROC_WDATA <= #0.1 0; //Write data
		PROC_WVALID <= #0.1 0; //Write valid
	  end
	  r = $fscanf(fin,"%d   %d    %x     %x\n", waitcycles, Cmd, Addr, Data);
	end
	$display("Waiting for the packets drain out");
	repeat (1000) @(negedge ACLK);
	$fclose(f_dump1);
	pattern_completed = 1;
	repeat (10) @(negedge ACLK);
	$display(" Pending write responses = %d \n", pending_write_responses);
	$display("Simulation Completed !! PID = %d", PID);
    repeat (10) @(negedge ACLK);
	$stop;
  end // end initial
  	
	
  //Response busy counter
  always @ (posedge ACLK)
  begin
    if (response_wait == 0) begin
      PROC_BREADY = 1;//Response ready
	  PROC_RREADY = 1;//Read ready
    end else begin
      response_wait <= response_wait - 1;
	  PROC_BREADY = 0;//Response ready
	  PROC_RREADY = 0;//Read ready
    end
  end

  
  //Tracking the write response counts
  always @ (posedge ACLK)
  begin
    if (((PROC_AWVALID == 1'b1) && (PROC_AWREADY == 1'b1))&&((PROC_BVALID == 1'b0) || (PROC_BREADY == 1'b0))) begin
      pending_write_responses = pending_write_responses + 1;
    end else if (((PROC_BVALID == 1'b1) && (PROC_BREADY == 1'b1))&&((PROC_AWVALID == 1'b0) || (PROC_AWREADY == 1'b0))) begin
      pending_write_responses = pending_write_responses - 1;
    end
  end
  
  // Open file to write read data
  //initial
    
  
  always @ (negedge ACLK)
  begin
    if ((PROC_RVALID == 1'b1) && (PROC_RREADY == 1'b1))
		begin
	  // Write the Received Data of the output file
	  #0.1 $fwrite(f_dump1, "%h\n", PROC_RDATA);
		end
		
		// if ((PROC_BVALID == 1'b1) && (PROC_BREADY == 1'b1))
		// begin
	  //Write the Received Data of the output file
	  // #0.1 $fwrite(f_dump, "%h\n", PROC_BRESP);
		// end
  end

endmodule 