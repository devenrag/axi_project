////////////////////////////////////////////////////////////
//// File: network.v
//// Author: Aparna Srinivasan
//// Version: 0.0
//// Date: 23rd April 2014
//// Description: This module is used to connect eight routers together.
////							Check the naming style in documents.
//// Updated by Yiheng Guo, 04/2015
//////////////////////////////////////////////////////////////

module network(
                    clk,
					reset,
            // Signals for dout to Master/Slave Interface
					r0_writevalid, 
					r1_writevalid, 
					r2_writevalid,
					r3_writevalid, 
					r4_writevalid, 
					r5_writevalid, 
					r6_writevalid, 
					r7_writevalid,
					r0_in_din5,
					r1_in_din5,
					r2_in_din5,
					r3_in_din5,
					r4_in_din5,
					r5_in_din5,
					r6_in_din5,
					r7_in_din5,
					r0_in_ready5,
					r1_in_ready5,
					r2_in_ready5,
					r3_in_ready5,
					r4_in_ready5,
					r5_in_ready5,
					r6_in_ready5,
					r7_in_ready5,
            // Signals for din from Master/Slave Interface
					r0_readvalid,
					r1_readvalid,
					r2_readvalid,
					r3_readvalid,
					r4_readvalid, 
					r5_readvalid,
					r6_readvalid,
					r7_readvalid,
                    r0_out_dout5,
                    r1_out_dout5,
                    r2_out_dout5,
                    r3_out_dout5,
                    r4_out_dout5,
                    r5_out_dout5,
                    r6_out_dout5,
                    r7_out_dout5,
                    r0_out_write,
                    r1_out_write,
                    r2_out_write,
                    r3_out_write,
                    r4_out_write,
                    r5_out_write,
                    r6_out_write,
                    r7_out_write
                    
					);

input clk, reset; 
input r0_readvalid, r1_readvalid, r2_readvalid, r3_readvalid, r4_readvalid, r5_readvalid, r6_readvalid,r7_readvalid;
input r0_writevalid, r1_writevalid, r2_writevalid, r3_writevalid, r4_writevalid, r5_writevalid, r6_writevalid, r7_writevalid;
input [159:0] r0_in_din5, r1_in_din5, r2_in_din5, r3_in_din5, r4_in_din5, r5_in_din5, r6_in_din5,  r7_in_din5;

output r0_in_ready5,r1_in_ready5,r2_in_ready5,r3_in_ready5,r4_in_ready5,r5_in_ready5,r6_in_ready5,r7_in_ready5;
output r0_out_write, r1_out_write, r2_out_write, r3_out_write, r4_out_write, r5_out_write, r6_out_write, r7_out_write;
output [159:0] r0_out_dout5, r1_out_dout5, r2_out_dout5, r3_out_dout5, r4_out_dout5, r5_out_dout5, r6_out_dout5, r7_out_dout5;

wire r0_in_valid1, r0_in_valid2, r0_in_valid3, r0_in_valid4;//these are inputs to input buffer of router
wire [31:0] r0_in_din1, r0_in_din2, r0_in_din3, r0_in_din4;
wire r0_in_ready1, r0_in_ready2, r0_in_ready3, r0_in_ready4;// these are outputs from input buffer of router
wire r0_out_ready1, r0_out_ready2, r0_out_ready3, r0_out_ready4, r0_out_ready5;//these are inputs to output buffer of router
wire r0_out_valid1, r0_out_valid2, r0_out_valid3, r0_out_valid4, r0_out_valid5;// these are outputs from ouput buffer of router
wire [31:0] r0_out_dout1, r0_out_dout2, r0_out_dout3, r0_out_dout4;





wire r1_in_valid1, r1_in_valid2, r1_in_valid3, r1_in_valid4;
wire [31:0] r1_in_din1, r1_in_din2, r1_in_din3, r1_in_din4;
wire r1_in_ready1, r1_in_ready2, r1_in_ready3, r1_in_ready4, r1_in_ready5;
wire r1_out_ready1, r1_out_ready2, r1_out_ready3, r1_out_ready4, r1_out_ready5;
wire [31:0] r1_out_dout1, r1_out_dout2, r1_out_dout3, r1_out_dout4;
wire r1_out_valid1, r1_out_valid2, r1_out_valid3, r1_out_valid4, r1_out_valid5;





wire r2_in_valid1, r2_in_valid2, r2_in_valid3, r2_in_valid4;
wire [31:0] r2_in_din1, r2_in_din2, r2_in_din3, r2_in_din4;
wire r2_in_ready1, r2_in_ready2, r2_in_ready3, r2_in_ready4;
wire r2_out_ready1, r2_out_ready2, r2_out_ready3, r2_out_ready4, r2_out_ready5;
wire r2_out_valid1, r2_out_valid2, r2_out_valid3, r2_out_valid4, r2_out_valid5;
wire [31:0] r2_out_dout1, r2_out_dout2, r2_out_dout3, r2_out_dout4;







wire r3_in_valid1, r3_in_valid2, r3_in_valid3, r3_in_valid4;
wire [31:0] r3_in_din1, r3_in_din2, r3_in_din3, r3_in_din4;
wire r3_in_ready1, r3_in_ready2, r3_in_ready3, r3_in_ready4;
wire r3_out_ready1, r3_out_ready2, r3_out_ready3, r3_out_ready4, r3_out_ready5;
wire r3_out_valid1, r3_out_valid2, r3_out_valid3, r3_out_valid4, r3_out_valid5;
wire [31:0] r3_out_dout1, r3_out_dout2, r3_out_dout3, r3_out_dout4;







wire r4_in_valid1, r4_in_valid2, r4_in_valid3, r4_in_valid4;
wire [31:0] r4_in_din1, r4_in_din2, r4_in_din3, r4_in_din4;
wire r4_in_ready1, r4_in_ready2, r4_in_ready3, r4_in_ready4;
wire r4_out_ready1, r4_out_ready2, r4_out_ready3, r4_out_ready4, r4_out_ready5;
wire r4_out_valid1, r4_out_valid2, r4_out_valid3, r4_out_valid4, r4_out_valid5;
wire [31:0] r4_out_dout1, r4_out_dout2, r4_out_dout3, r4_out_dout4;







wire r5_in_valid1, r5_in_valid2, r5_in_valid3, r5_in_valid4;
wire [31:0] r5_in_din1, r5_in_din2, r5_in_din3, r5_in_din4;
wire r5_in_ready1, r5_in_ready2, r5_in_ready3, r5_in_ready4;
wire r5_out_ready1, r5_out_ready2, r5_out_ready3, r5_out_ready4, r5_out_ready5;
wire r5_out_valid1, r5_out_valid2, r5_out_valid3, r5_out_valid4, r5_out_valid5;
wire [31:0] r5_out_dout1, r5_out_dout2, r5_out_dout3, r5_out_dout4;







wire r6_in_valid1, r6_in_valid2, r6_in_valid3, r6_in_valid4;
wire [31:0] r6_in_din1, r6_in_din2, r6_in_din3, r6_in_din4;
wire r6_in_ready1, r6_in_ready2, r6_in_ready3, r6_in_ready4;
wire r6_out_ready1, r6_out_ready2, r6_out_ready3, r6_out_ready4, r6_out_ready5;
wire r6_out_valid1, r6_out_valid2, r6_out_valid3, r6_out_valid4, r6_out_valid5;
wire [31:0] r6_out_dout1, r6_out_dout2, r6_out_dout3, r6_out_dout4;







wire r7_in_valid1, r7_in_valid2, r7_in_valid3, r7_in_valid4;
wire [31:0] r7_in_din1, r7_in_din2, r7_in_din3, r7_in_din4;
wire r7_in_ready1, r7_in_ready2, r7_in_ready3, r7_in_ready4;
wire r7_out_ready1, r7_out_ready2, r7_out_ready3, r7_out_ready4, r7_out_ready5;
wire r7_out_valid1, r7_out_valid2, r7_out_valid3, r7_out_valid4, r7_out_valid5;
wire [31:0] r7_out_dout1, r7_out_dout2, r7_out_dout3, r7_out_dout4;




assign r0_out_write  = r0_out_valid5 && r0_readvalid;
assign r1_out_write  = r1_out_valid5 && r1_readvalid ;
assign r2_out_write  = r2_out_valid5 && r2_readvalid ;
assign r3_out_write  = r3_out_valid5 && r3_readvalid ;
assign r4_out_write  = r4_out_valid5 && r4_readvalid ;
assign r5_out_write  = r5_out_valid5 && r5_readvalid ;
assign r6_out_write  = r6_out_valid5 && r6_readvalid ;
assign r7_out_write  = r7_out_valid5 && r7_readvalid ;



assign r1_in_valid2 = r0_out_valid1; // R0 sends to R1, R0 sends in xp direction so send1, R1 receives from xn direction so send2
assign r1_in_din2 = r0_out_dout1;
assign r0_out_ready1 = r1_in_ready2;

assign r2_in_valid2 = r1_out_valid1;// R1 sends to R2
assign r2_in_din2 = r1_out_dout1;
assign r1_out_ready1 = r2_in_ready2;

assign r3_in_valid2 = r2_out_valid1;//R2 sends to R3
assign r3_in_din2 = r2_out_dout1;
assign r2_out_ready1 = r3_in_ready2;

assign r5_in_valid2 = r4_out_valid1;//R4 sends to R5
assign r5_in_din2 = r4_out_dout1;
assign r4_out_ready1 = r5_in_ready2;

assign r6_in_valid2 = r5_out_valid1;// R5 sends to R6
assign r6_in_din2 = r5_out_dout1;
assign r5_out_ready1 = r6_in_ready2;

assign r7_in_valid2 = r6_out_valid1;//R6 sends to R7
assign r7_in_din2 = r6_out_dout1;
assign r6_out_ready1 = r7_in_ready2;

assign r4_in_valid3 = r0_out_valid4;//R0 sends to R4
assign r4_in_din3 = r0_out_dout4;
assign r0_out_ready4 = r4_in_ready3;

assign r5_in_valid3 = r1_out_valid4;//R1 sends to R5
assign r5_in_din3 = r1_out_dout4;
assign r1_out_ready4 = r5_in_ready3;

assign r6_in_valid3 = r2_out_valid4;//R2 sends to R6
assign r6_in_din3 = r2_out_dout4;
assign r2_out_ready4 = r6_in_ready3;

assign r7_in_valid3 = r3_out_valid4;//R3 sends to R7
assign r7_in_din3 = r3_out_dout4;
assign r3_out_ready4 = r7_in_ready3;

assign r0_in_valid1 = r1_out_valid2;//R1 sends to R0
assign r0_in_din1 = r1_out_dout2;
assign r1_out_ready2 = r0_in_ready1;

assign r1_in_valid1 = r2_out_valid2;//R2 sends to R1
assign r1_in_din1 = r2_out_dout2;
assign r2_out_ready2 = r1_in_ready1;

assign r2_in_valid1 = r3_out_valid2;//R3 sends to R2
assign r2_in_din1 = r3_out_dout2;
assign r3_out_ready2 = r2_in_ready1;

assign r4_in_valid1 = r5_out_valid2;//R5 sends to R4
assign r4_in_din1 = r5_out_dout2;
assign r5_out_ready2 = r4_in_ready1;

assign r5_in_valid1 = r6_out_valid2;//R6 sends to R5
assign r5_in_din1 = r6_out_dout2;
assign r6_out_ready2 = r5_in_ready1;

assign r6_in_valid1 = r7_out_valid2;//R7 sends to R6
assign r6_in_din1 = r7_out_dout2;
assign r7_out_ready2 = r6_in_ready1;


assign r0_in_valid4 = r4_out_valid3;// R4 sends to R0
assign r0_in_din4 = r4_out_dout3;
assign r4_out_ready3 = r0_in_ready4;

assign r1_in_valid4 = r5_out_valid3;//R5 sends to R1
assign r1_in_din4 = r5_out_dout3;
assign r5_out_ready3 = r1_in_ready4;


assign r2_in_valid4 = r6_out_valid3;//R6 sends to R2
assign r2_in_din4 = r6_out_dout3;
assign r6_out_ready3 = r2_in_ready4;

assign r3_in_valid4 = r7_out_valid3;//R7 sends to R3
assign r3_in_din4 = r7_out_dout3;
assign r7_out_ready3 = r3_in_ready4;

assign r0_out_ready5 = r0_readvalid;
assign r1_out_ready5 = r1_readvalid;
assign r2_out_ready5 = r2_readvalid;
assign r3_out_ready5 = r3_readvalid;
assign r4_out_ready5 = r4_readvalid;
assign r5_out_ready5 = r5_readvalid;
assign r6_out_ready5 = r6_readvalid;
assign r7_out_ready5 = r7_readvalid;




//R0 cannot receive from directions xn and yp, ie, 2 and 3 and it cannot send to directions 2 and 3

router R0(	.clk(clk), 
			.reset(reset), 
			.in_valid1(r0_in_valid1), 
			.in_valid2(1'b0), 
			.in_valid3(1'b0),
			.in_valid4(r0_in_valid4), 
			.in_valid5( r0_writevalid), 
			.in_din1(r0_in_din1), 
			.in_din2(r0_in_din2),
			.in_din3(r0_in_din3), 
			.in_din4(r0_in_din4),
			.in_din5(r0_in_din5),
			.in_ready1(r0_in_ready1), 
			.in_ready2(r0_in_ready2), 
			.in_ready3(r0_in_ready3), 
			.in_ready4(r0_in_ready4), 
			.in_ready5(r0_in_ready5),
			.out_ready1(r0_out_ready1),
			.out_ready2(1'b0),
			.out_ready3(1'b0), 
			.out_ready4(r0_out_ready4),
			.out_ready5(r0_out_ready5),
 			.out_valid1(r0_out_valid1),
 			.out_valid2(r0_out_valid2),
 			.out_valid3(r0_out_valid3), 
 			.out_valid4(r0_out_valid4),
 			.out_valid5(r0_out_valid5),
 			.out_dout1(r0_out_dout1),
 			.out_dout2(r0_out_dout2),
 			.out_dout3(r0_out_dout3),
 			.out_dout4(r0_out_dout4), 
 			.out_dout5(r0_out_dout5));
 			
//R1 cannot receive from direction yp ie, 3

router R1(	.clk(clk), 
			.reset(reset), 
			.in_valid1(r1_in_valid1), 
			.in_valid2(r1_in_valid2), 
			.in_valid3(1'b0),
			.in_valid4(r1_in_valid4), 
			.in_valid5( r1_writevalid), 
			.in_din1(r1_in_din1), 
			.in_din2(r1_in_din2),
			.in_din3(r1_in_din3), 
			.in_din4(r1_in_din4),
			.in_din5(r1_in_din5),
			.in_ready1(r1_in_ready1), 
			.in_ready2(r1_in_ready2), 
			.in_ready3(r1_in_ready3), 
			.in_ready4(r1_in_ready4), 
			.in_ready5(r1_in_ready5),
			.out_ready1(r1_out_ready1),
			.out_ready2(r1_out_ready2),
			.out_ready3(1'b0), 
			.out_ready4(r1_out_ready4),
			.out_ready5(r1_out_ready5),
 			.out_valid1(r1_out_valid1),
 			.out_valid2(r1_out_valid2),
 			.out_valid3(r1_out_valid3), 
 			.out_valid4(r1_out_valid4),
 			.out_valid5(r1_out_valid5),
 			.out_dout1(r1_out_dout1),
 			.out_dout2(r1_out_dout2),
 			.out_dout3(r1_out_dout3),
 			.out_dout4(r1_out_dout4), 
 			.out_dout5(r1_out_dout5));
 			
//R2 cannot receive from direction yp ie, 3

router R2(	.clk(clk), 
			.reset(reset), 
			.in_valid1(r2_in_valid1), 
			.in_valid2(r2_in_valid2), 
			.in_valid3(1'b0),
			.in_valid4(r2_in_valid4), 
			.in_valid5( r2_writevalid), 
			.in_din1(r2_in_din1), 
			.in_din2(r2_in_din2),
			.in_din3(r2_in_din3), 
			.in_din4(r2_in_din4),
			.in_din5(r2_in_din5),
			.in_ready1(r2_in_ready1), 
			.in_ready2(r2_in_ready2), 
			.in_ready3(r2_in_ready3), 
			.in_ready4(r2_in_ready4), 
			.in_ready5(r2_in_ready5),
			.out_ready1(r2_out_ready1),
			.out_ready2(r2_out_ready2),
			.out_ready3(1'b0), 
			.out_ready4(r2_out_ready4),
			.out_ready5(r2_out_ready5),
 			.out_valid1(r2_out_valid1),
 			.out_valid2(r2_out_valid2),
 			.out_valid3(r2_out_valid3), 
 			.out_valid4(r2_out_valid4),
 			.out_valid5(r2_out_valid5),
 			.out_dout1(r2_out_dout1),
 			.out_dout2(r2_out_dout2),
 			.out_dout3(r2_out_dout3),
 			.out_dout4(r2_out_dout4), 
 			.out_dout5(r2_out_dout5));
 			
//R3 cannot receive from directions xp and yp ie, 1 and 3

router R3(	.clk(clk), 
			.reset(reset), 
			.in_valid1(1'b0), 
			.in_valid2(r3_in_valid2), 
			.in_valid3(1'b0),
			.in_valid4(r3_in_valid4), 
			.in_valid5(r3_writevalid), 
			.in_din1(r3_in_din1), 
			.in_din2(r3_in_din2),
			.in_din3(r3_in_din3), 
			.in_din4(r3_in_din4),
			.in_din5(r3_in_din5),
			.in_ready1(r3_in_ready1), 
			.in_ready2(r3_in_ready2), 
			.in_ready3(r3_in_ready3), 
			.in_ready4(r3_in_ready4), 
			.in_ready5(r3_in_ready5),
			.out_ready1(1'b0),
			.out_ready2(r3_out_ready2),
			.out_ready3(1'b0), 
			.out_ready4(r3_out_ready4),
			.out_ready5(r3_out_ready5),
 			.out_valid1(r3_out_valid1),
 			.out_valid2(r3_out_valid2),
 			.out_valid3(r3_out_valid3), 
 			.out_valid4(r3_out_valid4),
 			.out_valid5(r3_out_valid5),
 			.out_dout1(r3_out_dout1),
 			.out_dout2(r3_out_dout2),
 			.out_dout3(r3_out_dout3),
 			.out_dout4(r3_out_dout4), 
 			.out_dout5(r3_out_dout5));

//R4 cannot receive from directions xn and yn ie, 2 and 4

router R4(	.clk(clk), 
			.reset(reset), 
			.in_valid1(r4_in_valid1), 
			.in_valid2(1'b0), 
			.in_valid3(r4_in_valid3),
			.in_valid4(1'b0), 
			.in_valid5(r4_writevalid), 
			.in_din1(r4_in_din1), 
			.in_din2(r4_in_din2),
			.in_din3(r4_in_din3), 
			.in_din4(r4_in_din4),
			.in_din5(r4_in_din5),
			.in_ready1(r4_in_ready1), 
			.in_ready2(r4_in_ready2), 
			.in_ready3(r4_in_ready3), 
			.in_ready4(r4_in_ready4), 
			.in_ready5(r4_in_ready5),
			.out_ready1(r4_out_ready1),
			.out_ready2(1'b0),
			.out_ready3(r4_out_ready3), 
			.out_ready4(1'b0),
			.out_ready5(r4_out_ready5),
 			.out_valid1(r4_out_valid1),
 			.out_valid2(r4_out_valid2),
 			.out_valid3(r4_out_valid3), 
 			.out_valid4(r4_out_valid4),
 			.out_valid5(r4_out_valid5),
 			.out_dout1(r4_out_dout1),
 			.out_dout2(r4_out_dout2),
 			.out_dout3(r4_out_dout3),
 			.out_dout4(r4_out_dout4), 
 			.out_dout5(r4_out_dout5));
 			
//R5 cannot receive from directions yn ie, 4

router R5(	.clk(clk), 
			.reset(reset), 
			.in_valid1(r5_in_valid1), 
			.in_valid2(r5_in_valid2), 
			.in_valid3(r5_in_valid3),
			.in_valid4(1'b0), 
			.in_valid5(r5_writevalid), 
			.in_din1(r5_in_din1), 
			.in_din2(r5_in_din2),
			.in_din3(r5_in_din3), 
			.in_din4(r5_in_din4),
			.in_din5(r5_in_din5),
			.in_ready1(r5_in_ready1), 
			.in_ready2(r5_in_ready2), 
			.in_ready3(r5_in_ready3), 
			.in_ready4(r5_in_ready4), 
			.in_ready5(r5_in_ready5),
			.out_ready1(r5_out_ready1),
			.out_ready2(r5_out_ready2),
			.out_ready3(r5_out_ready3), 
			.out_ready4(1'b0),
			.out_ready5(r5_out_ready5),
 			.out_valid1(r5_out_valid1),
 			.out_valid2(r5_out_valid2),
 			.out_valid3(r5_out_valid3), 
 			.out_valid4(r5_out_valid4),
 			.out_valid5(r5_out_valid5),
 			.out_dout1(r5_out_dout1),
 			.out_dout2(r5_out_dout2),
 			.out_dout3(r5_out_dout3),
 			.out_dout4(r5_out_dout4), 
 			.out_dout5(r5_out_dout5));
 			
//R6 cannot receive from directions yn ie, 4
 			
router R6(	.clk(clk), 
			.reset(reset), 
			.in_valid1(r6_in_valid1), 
			.in_valid2(r6_in_valid2), 
			.in_valid3(r6_in_valid3),
			.in_valid4(1'b0), 
			.in_valid5(r6_writevalid), 
			.in_din1(r6_in_din1), 
			.in_din2(r6_in_din2),
			.in_din3(r6_in_din3), 
			.in_din4(r6_in_din4),
			.in_din5(r6_in_din5),
			.in_ready1(r6_in_ready1), 
			.in_ready2(r6_in_ready2), 
			.in_ready3(r6_in_ready3), 
			.in_ready4(r6_in_ready4), 
			.in_ready5(r6_in_ready5),
			.out_ready1(r6_out_ready1),
			.out_ready2(r6_out_ready2),
			.out_ready3(r6_out_ready3), 
			.out_ready4(1'b0),
			.out_ready5(r6_out_ready5),
 			.out_valid1(r6_out_valid1),
 			.out_valid2(r6_out_valid2),
 			.out_valid3(r6_out_valid3), 
 			.out_valid4(r6_out_valid4),
 			.out_valid5(r6_out_valid5),
 			.out_dout1(r6_out_dout1),
 			.out_dout2(r6_out_dout2),
 			.out_dout3(r6_out_dout3),
 			.out_dout4(r6_out_dout4), 
 			.out_dout5(r6_out_dout5));
 			
//R7 cannot receive from directions xp and yn ie, 1 and 4

router R7(	.clk(clk), 
			.reset(reset), 
			.in_valid1(1'b0), 
			.in_valid2(r7_in_valid2), 
			.in_valid3(r7_in_valid3),
			.in_valid4(1'b0), 
			.in_valid5( r7_writevalid), 
			.in_din1(r7_in_din1), 
			.in_din2(r7_in_din2),
			.in_din3(r7_in_din3), 
			.in_din4(r7_in_din4),
			.in_din5(r7_in_din5),
			.in_ready1(r7_in_ready1), 
			.in_ready2(r7_in_ready2), 
			.in_ready3(r7_in_ready3), 
			.in_ready4(r7_in_ready4), 
			.in_ready5(r7_in_ready5),
			.out_ready1(1'b0),
			.out_ready2(r7_out_ready2),
			.out_ready3(r7_out_ready3), 
			.out_ready4(1'b0),
			.out_ready5(r7_out_ready5),
 			.out_valid1(r7_out_valid1),
 			.out_valid2(r7_out_valid2),
 			.out_valid3(r7_out_valid3), 
 			.out_valid4(r7_out_valid4),
 			.out_valid5(r7_out_valid5),
 			.out_dout1(r7_out_dout1),
 			.out_dout2(r7_out_dout2),
 			.out_dout3(r7_out_dout3),
 			.out_dout4(r7_out_dout4), 
 			.out_dout5(r7_out_dout5));

endmodule