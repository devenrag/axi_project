////--------------------------------------------------------------////
// File 		 : tb_axi_slave_mem.v
// Author		 : Andrew Varghese
// Version       : 0.0
// Date          : 19th April 2014
// Description	 : This module will read and write to memory based on the AXI transactions.
////--------------------------------------------------------------////

`timescale 1 ns / 1 ps

module axi_slave_mem(
  input ACLK, //global clock
  input ARESETn, //global reset
  
  //write address channel
  output reg MEM_AWREADY,//Write address ready
  input [2:0] MEM_AWID, // Write address ID 
  input [31:0] MEM_AWADDR, //Write address 
  input MEM_AWVALID,//Write address ready
  
  //Write data channel
  output reg MEM_WREADY,//Write ready
  input [2:0] MEM_WID,//Write ID tag
  input [127:0] MEM_WDATA,//Write data
  input MEM_WVALID,//Write valid
  
  //write response channel
  output reg [2:0] MEM_BID,//Response ID tag
  output reg [1:0] MEM_BRESP,//Write response
  output reg MEM_BVALID,//Write response valid
  input MEM_BREADY,//Response ready
  
  //read address channel
  output reg MEM_ARREADY,//Write address ready
  input [2:0] MEM_ARID, // Read address ID 
  input [31:0] MEM_ARADDR, //Read address 
  input MEM_ARVALID,//Read address ready
  
  //read data channel
  output reg [2:0] MEM_RID,//Read ID tag
  output reg [127:0] MEM_RDATA,//Read data
  output reg MEM_RVALID,//Read valid
  input MEM_RREADY//Read ready

);

  reg [127:0] memory [63:0];
	// addr to index memory
	// ADDR stores MEM_AWADDR and will be sent back as write_response
  reg [5:0] addr;
	reg [31:0] ADDR;
  reg [7:0] count;
  reg got_w_address;
  reg sent_w_response;
  reg got_r_address;
  reg sent_r_response;
  reg rd_wrn_arbitrator;
wire rd_ready;
wire wr_ready;
  
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
      rd_wrn_arbitrator <= 0;
    end else begin
      rd_wrn_arbitrator <= ~ rd_wrn_arbitrator;
    end
  end
  
  assign rd_ready = (got_w_address == 0) && (got_r_address == 0) && (rd_wrn_arbitrator == 1);
  assign wr_ready = (got_w_address == 0) && (got_r_address == 0) && (rd_wrn_arbitrator == 0);
  
  //Address write ready given 1 clock after valid
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
      MEM_AWREADY <= 0;
    end else if ((wr_ready == 1)&&(MEM_AWVALID == 1)) begin
      MEM_AWREADY <= 1;
    end else begin
      MEM_AWREADY <= 0;
    end
  end
  
  //Write and read addresses
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
  	  addr <= 0;
			ADDR <= 0;
      got_r_address <= 0;
      got_w_address <= 0;
    end else if ((wr_ready == 1)&&(MEM_AWVALID == 1)) begin
  	  addr <= MEM_AWADDR[11:6];
			ADDR <= MEM_AWADDR;
      got_w_address <= 1;
    end else if ((rd_ready == 1)&&(MEM_ARVALID == 1)) begin
      addr <= MEM_ARADDR[11:6];
      got_r_address <= 1;
    end else if (sent_w_response == 1) begin
        got_w_address <= 0;
    end else if (sent_r_response == 1) begin
        got_r_address <= 0;
    end
  end
  
  
  //Write data and response
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
      MEM_WREADY <= 0;
    end else if ((MEM_WVALID == 1)&&(got_w_address == 1)) begin
      sent_w_response <= 0;
      MEM_WREADY <= 0;
      if (count > 0) begin
        count <= count - 1;
        //MEM_WREADY <= 0;
        if (count == 1) begin
          MEM_WREADY <= 1;
          sent_w_response <= 1;
        end
      end //else begin
    end else if (got_w_address == 0) begin
        sent_w_response <= 0;
        MEM_WREADY <= 0;
  	    count <= 20; //constant count value write latency
    end
  end
  
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
  	  MEM_BID <= 0;
  	  MEM_BRESP <= 0;
      MEM_BVALID <= 0;
    end else if ((MEM_WVALID == 1)&&(MEM_WREADY == 1)) begin
  	  memory[addr] <= MEM_WDATA;
			MEM_BRESP <= 0;
  	  MEM_BVALID <= 1;
    end else if (MEM_BREADY == 1) begin
      MEM_BVALID <= 0;
    end
  end
  
  //Read address
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
      MEM_ARREADY <= 0;
    end else if ((rd_ready == 1)&&(MEM_ARVALID == 1)&&(MEM_ARREADY != 1)) begin //MEM_ARREADY != 1 actually not required
      MEM_ARREADY <= 1;
    end else begin
      MEM_ARREADY <= 0;
    end
  end
  
  //Read data
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
      MEM_RVALID <= 0;
      MEM_RID <= 0;
      sent_r_response <= 0;
      //MEM_RDATA <= 0;
    end else if ((MEM_ARVALID == 1)&&(MEM_ARREADY == 1)) begin
      //MEM_RDATA <= memory[addr];
  	MEM_RVALID <= 1;
    sent_r_response <= 1;
    end else if (MEM_RREADY == 1) begin
      MEM_RVALID <= 0;
      sent_r_response <= 0;
      //MEM_RDATA <= 0;
    end
  end
  
  always @ (posedge ACLK)
  begin
    if (ARESETn == 0) begin
      MEM_RDATA <= 0;
    end else begin
      MEM_RDATA <= memory[addr];
    end
  end

endmodule 