////--------------------------------------------------------------////
// File 		 : tb_top.v
// Author		 : Andrew Varghese
// Version       : 0.0
// Date          : 19th April 2014
// Description	 : This module will generate clock and reset and also dumps the
//                  memory when simulation is over.
////--------------------------------------------------------------////


`timescale 1 ns / 1 ps

module tb ();

	reg ARESETn;
	reg ACLK;
	
   // define clocks
   initial ACLK = 0;
   always #5 ACLK = ~ACLK; // 100MHz
	
  // define reset
	initial begin
		ARESETn = 1'b0;
		repeat (10) @(posedge ACLK);
		#5;
		ARESETn = 1'b1;
	end
	
	// clock counter
	integer			clk_cnt = 0;
	always @ (posedge ACLK) begin
		clk_cnt <= clk_cnt + 1;
	end
	
	///////////////////////////////////////
	//	file interface
	//////////////////////////////////////
	integer			 fout_mem4;
	integer      fout_mem5;
	integer      fout_mem6;	
	integer      fout_mem7;
	initial begin
		fout_mem4 = $fopen("./results/output_memory4.dump");
		fout_mem5 = $fopen("./results/output_memory5.dump");
		fout_mem6 = $fopen("./results/output_memory6.dump");
		fout_mem7 = $fopen("./results/output_memory7.dump");
		
		@ (posedge ACLK)
        while ((cmp_inst.proc0_inst.pattern_completed==0)&&(cmp_inst.proc1_inst.pattern_completed==0)&&
								(cmp_inst.proc2_inst.pattern_completed==0)&&(cmp_inst.proc3_inst.pattern_completed==0)
				) begin
          @ (posedge ACLK);
        end
	    TASK_MEM_FOUT;
	end

	//////////////////////////////////////
	//	instances
	///////////////////////////////////////
	cmp		cmp_inst (
		.ACLK			(ACLK			),
		.ARESETn		(ARESETn		)
                                        
	);
	

	/////////////////////////////////////////////////////
	// test-bench tasks
	/////////////////////////////////////////////////////
	
	task TASK_MEM_FOUT;
		integer			addr;
	begin
		addr =0;
        // file-write main memory data
		$fwrite(fout_mem6, "**********************\n");
		$fwrite(fout_mem6, "* Main memory\n");
		$fwrite(fout_mem6, "**********************\n");
		$fwrite(fout_mem6, "  addr | data\n");

		for (addr = 0; addr < 64; addr = addr + 1) begin
			$fwrite(fout_mem6, "%03d | %32X\n", addr, cmp_inst.mem6_inst.memory[addr]);
		end
		
		addr =0;
        // file-write main memory data
		$fwrite(fout_mem4, "**********************\n");
		$fwrite(fout_mem4, "* Main memory\n");
		$fwrite(fout_mem4, "**********************\n");
		$fwrite(fout_mem4, "  addr | data\n");

		for (addr = 0; addr < 64; addr = addr + 1) begin
			$fwrite(fout_mem4, "%03d | %32X\n", addr, cmp_inst.mem4_inst.memory[addr]);
		end
        $display(" Write to memory completed \n");
				
		addr =0;
        // file-write main memory data
		$fwrite(fout_mem5, "**********************\n");
		$fwrite(fout_mem5, "* Main memory\n");
		$fwrite(fout_mem5, "**********************\n");
		$fwrite(fout_mem5, "  addr | data\n");

		for (addr = 0; addr < 64; addr = addr + 1) begin
			$fwrite(fout_mem5, "%03d | %32X\n", addr, cmp_inst.mem5_inst.memory[addr]);
		end
        $display(" Write to memory completed \n");
				
				addr =0;
        // file-write main memory data
		$fwrite(fout_mem7, "**********************\n");
		$fwrite(fout_mem7, "* Main memory\n");
		$fwrite(fout_mem7, "**********************\n");
		$fwrite(fout_mem7, "  addr | data\n");

		for (addr = 0; addr < 64; addr = addr + 1) begin
			$fwrite(fout_mem7, "%03d | %32X\n", addr, cmp_inst.mem7_inst.memory[addr]);
		end
        $display(" Write to memory completed \n");
//		for (addr = 0; addr < 64; addr = addr + 1) begin
//			$fwrite(fout_mem, "%03d | %32X\n", addr, cmp_inst.mem1_inst.memory[addr]);
//		end
//		for (addr = 0; addr < 64; addr = addr + 1) begin
//			$fwrite(fout_mem, "%03d | %32X\n", addr, cmp_inst.mem2_inst.memory[addr]);
//		end
//		for (addr = 0; addr < 64; addr = addr + 1) begin
//			$fwrite(fout_mem, "%03d | %32X\n", addr, cmp_inst.mem3_inst.memory[addr]);
//		end
	end
	endtask
	
endmodule
	
	
	
	
	
	
	
	
	
	
