// -------------------------------------------------------------
// Name: 				Arbitration_logic.v
// Author: 			Yiheng
// Date: 				02/2015
// Description: This arbiter uses LRU arbitration scheme. 
// 							The grant should be high for 5 clock, because there are 5 phits needing to transfer.
// --------------------------------------------------------------

module Arbiter(clk,reset,Req1,Req2,Req3,Req4,Gnt1,Gnt2,Gnt3,Gnt4);
input clk,reset,Req1,Req2,Req3,Req4;
output Gnt1,Gnt2,Gnt3,Gnt4;

wire Gnt1,Gnt2,Gnt3,Gnt4;
// lru queue, lru[0] indicates the highest priority request
reg [1:0] lru[3:0];
// counter to indicate how many clocks the active request have lasted,
// if counter=4, clear it
reg [2:0] counter;
// indicate the granted req is active
reg active; 
reg [3:0] reserve;

// just for coding convenience
wire [3:0] req;
reg [3:0] gnt;
assign req[0] = Req1;
assign req[1] = Req2;
assign req[2] = Req3;
assign req[3] = Req4;
assign Gnt1 = gnt[0];
assign Gnt2 = gnt[1];
assign Gnt3 = gnt[2];
assign Gnt4 = gnt[3];

integer i;

always@(posedge clk)
begin
if(reset==0)
	begin
	// set default value for lru queue
	for(i=0;i<4;i=i+1)
		begin
		lru[i] <= i;
		gnt[i] <= 0;
		reserve[i] <= 0;
		end
	
	counter <= 0;
	end
else
	begin
	// update counter, when active=1
	if(active==1)
		begin
		if(counter==4)
			begin
			counter <= 0;
			end
		else
			begin
			counter <= counter + 1;
			end
		end
		
	// update lru, when counter=4 and active=1
	if(counter==4 && active ==1)
		begin
		
		if(req[lru[0]]==1)
			begin
			lru[0] <= lru[1];
			lru[1] <= lru[2];
			lru[2] <= lru[3];
			lru[3] <= lru[0];
			end
		else if (req[lru[1]]==1)
			begin
			lru[1] <= lru[2];
			lru[2] <= lru[3];
			lru[3] <= lru[1];
			end
		else if(req[lru[2]]==1)
			begin
			
			lru[2] <= lru[3];
			lru[3] <= lru[2];
			end
		else if(req[lru[3]]==1) 
			begin
			lru[3] <= lru[3];
			end
		end

	if(counter==0)
		begin

		reserve <= 0;
		
		if(req[lru[0]]==1)
			begin
			reserve[lru[0]] <= 1;
			end
		else if (req[lru[1]]==1)
			begin
			reserve[lru[1]] <= 1;
			end
		else if(req[lru[2]]==1)
			begin
			reserve[lru[2]] <= 1;
			end
		else if(req[lru[3]]==1)
			begin		
			reserve[lru[3]] <= 1;
			end		
		end

	end
end



always@(*)
begin
active = 0;

if(counter==0)
	begin
	active = req[0]||req[1]||req[2]||req[3];
	end
else
	begin
	if(reserve[0]==1)
		begin
		active = req[0];
		end
	else if(reserve[1]==1)
		begin
		active = req[1];
		end
	else if(reserve[2]==1)
		begin
		active = req[2];
		end
	else if(reserve[3]==1)
		begin
		active = req[3];
		end
	end

	// give gnts
	// set gnts default value
for(i=0;i<4;i=i+1)
	begin
	gnt[i] <= 0;
	end
	
if(counter==0)
	begin

	if(req[lru[0]]==1)
		begin
		gnt[lru[0]] <= 1;
		end
	else if (req[lru[1]]==1)
		begin
		gnt[lru[1]] <= 1;
		end
	else if(req[lru[2]]==1)
		begin
		gnt[lru[2]] <= 1;
		end
	else if(req[lru[3]]==1)
		begin
		gnt[lru[3]] <= 1;
		end		
	end
else
	begin
	if(reserve[0]==1)
		begin
		gnt[0] <= active;
		end
	else if(reserve[1]==1)
		begin
		gnt[1] <= active;
		end
	else if(reserve[2]==1)
		begin
		gnt[2] <= active;
		end
	else if(reserve[3]==1)
		begin
		gnt[3] <= active;
		end
	end

end
endmodule 