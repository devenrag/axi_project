////--------------------------------------------------------------////
// File 		 : cmp.v
// Author		 : Andrew Varghese
// Version       : 0.0
// Date          : 19th April 2014
// Description	 : This module will instantiate all the Processor, Memory and Slave modules.
//                 This will be the top module for synthesis if axi master and slave devices
//                 are replaced by actual processors.\\
// Updated by Yiheng Guo, 04/2015
////--------------------------------------------------------------////


`timescale 1 ns / 1 ps

module	cmp (
		 input ACLK,
		 input ARESETn
	);




wire 					PROC3_AWREADY,	PROC2_AWREADY,	PROC1_AWREADY,	PROC0_AWREADY;
wire [2:0] 		PROC3_AWID,			PROC2_AWID,			PROC1_AWID,			PROC0_AWID;
wire [31:0] 	PROC3_AWADDR,		PROC2_AWADDR,		PROC1_AWADDR,		PROC0_AWADDR;
wire 					PROC3_AWVALID,	PROC2_AWVALID,	PROC1_AWVALID,	PROC0_AWVALID;
wire 					PROC3_WREADY,		PROC2_WREADY,		PROC1_WREADY,		PROC0_WREADY;
wire [2:0] 		PROC3_WID,			PROC2_WID,			PROC1_WID,			PROC0_WID;
wire [127:0] 	PROC3_WDATA,		PROC2_WDATA,		PROC1_WDATA,		PROC0_WDATA;
wire 					PROC3_WVALID,		PROC2_WVALID,		PROC1_WVALID,		PROC0_WVALID;
wire [2:0] 		PROC3_BID,			PROC2_BID,			PROC1_BID,			PROC0_BID;
wire [1:0] 		PROC3_BRESP,		PROC2_BRESP,		PROC1_BRESP,		PROC0_BRESP;
wire 					PROC3_BVALID,		PROC2_BVALID,		PROC1_BVALID,		PROC0_BVALID;
wire 					PROC3_BREADY,		PROC2_BREADY,		PROC1_BREADY,		PROC0_BREADY;
wire 					PROC3_ARREADY,	PROC2_ARREADY,	PROC1_ARREADY,	PROC0_ARREADY;
wire [2:0] 		PROC3_ARID,			PROC2_ARID,			PROC1_ARID,			PROC0_ARID;
wire [31:0] 	PROC3_ARADDR,		PROC2_ARADDR,		PROC1_ARADDR,		PROC0_ARADDR;
wire 					PROC3_ARVALID,	PROC2_ARVALID,	PROC1_ARVALID,	PROC0_ARVALID;
wire [2:0] 		PROC3_RID,			PROC2_RID,			PROC1_RID,			PROC0_RID;
wire [127:0] 	PROC3_RDATA,		PROC2_RDATA,		PROC1_RDATA,		PROC0_RDATA;
wire 					PROC3_RVALID,		PROC2_RVALID,		PROC1_RVALID,		PROC0_RVALID;
wire 					PROC3_RREADY,		PROC2_RREADY,		PROC1_RREADY,		PROC0_RREADY;

wire					SLAVE4_AWREADY, SLAVE5_AWREADY, SLAVE6_AWREADY,	 SLAVE7_AWREADY; 
wire [2:0] 		SLAVE4_AWID, 		SLAVE5_AWID, 		SLAVE6_AWID,     SLAVE7_AWID; 		
wire [31:0] 	SLAVE4_AWADDR,	SLAVE5_AWADDR,	SLAVE6_AWADDR,   SLAVE7_AWADDR;	
wire 					SLAVE4_AWVALID,	SLAVE5_AWVALID,	SLAVE6_AWVALID,  SLAVE7_AWVALID;	
wire 					SLAVE4_WREADY,	SLAVE5_WREADY,	SLAVE6_WREADY,   SLAVE7_WREADY;	
wire [2:0] 		SLAVE4_WID,			SLAVE5_WID,			SLAVE6_WID,      SLAVE7_WID;			
wire [127:0] 	SLAVE4_WDATA,		SLAVE5_WDATA,		SLAVE6_WDATA,    SLAVE7_WDATA;		
wire 					SLAVE4_WVALID,	SLAVE5_WVALID,	SLAVE6_WVALID,   SLAVE7_WVALID;	
wire [2:0] 		SLAVE4_BID,			SLAVE5_BID,			SLAVE6_BID,      SLAVE7_BID;			
wire [1:0] 		SLAVE4_BRESP,		SLAVE5_BRESP,		SLAVE6_BRESP,    SLAVE7_BRESP;		
wire 					SLAVE4_BVALID,	SLAVE5_BVALID,	SLAVE6_BVALID,   SLAVE7_BVALID;	
wire 					SLAVE4_BREADY,	SLAVE5_BREADY,	SLAVE6_BREADY,   SLAVE7_BREADY;	
wire 					SLAVE4_ARREADY,	SLAVE5_ARREADY,	SLAVE6_ARREADY,  SLAVE7_ARREADY;	
wire [2:0]		SLAVE4_ARID,		SLAVE5_ARID,		SLAVE6_ARID,     SLAVE7_ARID;		
wire [31:0] 	SLAVE4_ARADDR,	SLAVE5_ARADDR,	SLAVE6_ARADDR,   SLAVE7_ARADDR;	
wire 					SLAVE4_ARVALID,	SLAVE5_ARVALID,	SLAVE6_ARVALID,  SLAVE7_ARVALID;	
wire [2:0] 		SLAVE4_RID,			SLAVE5_RID,			SLAVE6_RID,      SLAVE7_RID;			
wire [127:0] 	SLAVE4_RDATA,		SLAVE5_RDATA,		SLAVE6_RDATA,    SLAVE7_RDATA;		
wire 					SLAVE4_RVALID,	SLAVE5_RVALID,	SLAVE6_RVALID,   SLAVE7_RVALID;	
wire 					SLAVE4_RREADY,	SLAVE5_RREADY,	SLAVE6_RREADY,   SLAVE7_RREADY;	


   /////////////////////////////////////////
   // Interconnecting Modules
   //////////////////////////////////////////
   axi_master_proc #(.PID(0)) proc0_inst(
		.ACLK 					(ACLK), //global clock
		.ARESETn 				(ARESETn), //global reset
		//write address channel
		.PROC_AWREADY		(PROC0_AWREADY),//Write address ready
		.PROC_AWID			(PROC0_AWID), // Write address ID //decide on the ID
		.PROC_AWADDR		(PROC0_AWADDR), //Write address //decide on the address width
		.PROC_AWVALID		(PROC0_AWVALID),//Write address ready
		//Write data channel
		.PROC_WREADY		(PROC0_WREADY),//Write ready
		.PROC_WID				(PROC0_WID),//Write ID tag
		.PROC_WDATA			(PROC0_WDATA),//Write data
		.PROC_WVALID		(PROC0_WVALID),//Write valid
		//write response channel
		.PROC_BID				(PROC0_BID),//Response ID tag
		.PROC_BRESP			(PROC0_BRESP),//Write response
		.PROC_BVALID		(PROC0_BVALID),//Write response valid
		.PROC_BREADY		(PROC0_BREADY),//Response ready
		//read address channel
		.PROC_ARREADY		(PROC0_ARREADY),//Write address ready
		.PROC_ARID			(PROC0_ARID), // Read address ID //decide on the ID
		.PROC_ARADDR		(PROC0_ARADDR), //Read address //decide on the address width
		.PROC_ARVALID		(PROC0_ARVALID),//Read address ready
		//read data channel
		.PROC_RID				(PROC0_RID),//Read ID tag
		.PROC_RDATA			(PROC0_RDATA),//Read data
		.PROC_RVALID		(PROC0_RVALID),//Read valid
		.PROC_RREADY		(PROC0_RREADY)//Read ready
);

   axi_master_proc #(.PID(1)) proc1_inst(
		.ACLK 					(ACLK), //global clock
		.ARESETn 				(ARESETn), //global reset
		//write address channel
		.PROC_AWREADY		(PROC1_AWREADY),//Write address ready
		.PROC_AWID			(PROC1_AWID), // Write address ID //decide on the ID
		.PROC_AWADDR		(PROC1_AWADDR), //Write address //decide on the address width
		.PROC_AWVALID		(PROC1_AWVALID),//Write address ready
		//Write data channel
		.PROC_WREADY		(PROC1_WREADY),//Write ready
		.PROC_WID				(PROC1_WID),//Write ID tag
		.PROC_WDATA			(PROC1_WDATA),//Write data
		.PROC_WVALID		(PROC1_WVALID),//Write valid
		//write response channel
		.PROC_BID				(PROC1_BID),//Response ID tag
		.PROC_BRESP			(PROC1_BRESP),//Write response
		.PROC_BVALID		(PROC1_BVALID),//Write response valid
		.PROC_BREADY		(PROC1_BREADY),//Response ready
		//read address channel
		.PROC_ARREADY		(PROC1_ARREADY),//Write address ready
		.PROC_ARID			(PROC1_ARID), // Read address ID //decide on the ID
		.PROC_ARADDR		(PROC1_ARADDR), //Read address //decide on the address width
		.PROC_ARVALID		(PROC1_ARVALID),//Read address ready
		//read data channel
		.PROC_RID				(PROC1_RID),//Read ID tag
		.PROC_RDATA			(PROC1_RDATA),//Read data
		.PROC_RVALID		(PROC1_RVALID),//Read valid
		.PROC_RREADY		(PROC1_RREADY)//Read ready
);

   axi_master_proc  #(.PID(2)) proc2_inst(
		.ACLK 					(ACLK), //global clock
		.ARESETn 				(ARESETn), //global reset
		//write address channel
		.PROC_AWREADY		(PROC2_AWREADY),//Write address ready
		.PROC_AWID			(PROC2_AWID), // Write address ID //decide on the ID
		.PROC_AWADDR		(PROC2_AWADDR), //Write address //decide on the address width
		.PROC_AWVALID		(PROC2_AWVALID),//Write address ready
		//Write data channel
		.PROC_WREADY		(PROC2_WREADY),//Write ready
		.PROC_WID				(PROC2_WID),//Write ID tag
		.PROC_WDATA			(PROC2_WDATA),//Write data
		.PROC_WVALID		(PROC2_WVALID),//Write valid
		//write response channel
		.PROC_BID				(PROC2_BID),//Response ID tag
		.PROC_BRESP			(PROC2_BRESP),//Write response
		.PROC_BVALID		(PROC2_BVALID),//Write response valid
		.PROC_BREADY		(PROC2_BREADY),//Response ready
		//read address channel
		.PROC_ARREADY		(PROC2_ARREADY),//Write address ready
		.PROC_ARID			(PROC2_ARID), // Read address ID //decide on the ID
		.PROC_ARADDR		(PROC2_ARADDR), //Read address //decide on the address width
		.PROC_ARVALID		(PROC2_ARVALID),//Read address ready
		//read data channel
		.PROC_RID				(PROC2_RID),//Read ID tag
		.PROC_RDATA			(PROC2_RDATA),//Read data
		.PROC_RVALID		(PROC2_RVALID),//Read valid
		.PROC_RREADY		(PROC2_RREADY)//Read ready
);

   axi_master_proc  #(.PID(3)) proc3_inst(
		.ACLK 					(ACLK), //global clock
		.ARESETn 				(ARESETn), //global reset
		//write address channel
		.PROC_AWREADY		(PROC3_AWREADY),//Write address ready
		.PROC_AWID			(PROC3_AWID), // Write address ID //decide on the ID
		.PROC_AWADDR		(PROC3_AWADDR), //Write address //decide on the address width
		.PROC_AWVALID		(PROC3_AWVALID),//Write address ready
		//Write data channel
		.PROC_WREADY		(PROC3_WREADY),//Write ready
		.PROC_WID				(PROC3_WID),//Write ID tag
		.PROC_WDATA			(PROC3_WDATA),//Write data
		.PROC_WVALID		(PROC3_WVALID),//Write valid
		//write response channel
		.PROC_BID				(PROC3_BID),//Response ID tag
		.PROC_BRESP			(PROC3_BRESP),//Write response
		.PROC_BVALID		(PROC3_BVALID),//Write response valid
		.PROC_BREADY		(PROC3_BREADY),//Response ready
		//read address channel
		.PROC_ARREADY		(PROC3_ARREADY),//Write address ready
		.PROC_ARID			(PROC3_ARID), // Read address ID //decide on the ID
		.PROC_ARADDR		(PROC3_ARADDR), //Read address //decide on the address width
		.PROC_ARVALID		(PROC3_ARVALID),//Read address ready
		//read data channel
		.PROC_RID				(PROC3_RID),//Read ID tag
		.PROC_RDATA			(PROC3_RDATA),//Read data
		.PROC_RVALID		(PROC3_RVALID),//Read valid
		.PROC_RREADY		(PROC3_RREADY)//Read ready
);

axi_slave_mem   mem4_inst(
		.ACLK 				(ACLK), //global clock
		.ARESETn 			(ARESETn), //global reset
		//write address channel
		.MEM_AWREADY	(SLAVE4_AWREADY),//Write address ready
		.MEM_AWID			(SLAVE4_AWID), // Write address ID //decide on the ID
		.MEM_AWADDR		(SLAVE4_AWADDR), //Write address //decide on the address width
		.MEM_AWVALID	(SLAVE4_AWVALID),//Write address ready
		//Write data channel
		.MEM_WREADY		(SLAVE4_WREADY),//Write ready
		.MEM_WID			(SLAVE4_WID),//Write ID tag
		.MEM_WDATA		(SLAVE4_WDATA),//Write data
		.MEM_WVALID		(SLAVE4_WVALID),//Write valid
		//write response channel
		.MEM_BID			(SLAVE4_BID),//Response ID tag
		.MEM_BRESP		(SLAVE4_BRESP),//Write response
		.MEM_BVALID		(SLAVE4_BVALID),//Write response valid
		.MEM_BREADY		(SLAVE4_BREADY),//Response ready
		//read address channel
		.MEM_ARREADY	(SLAVE4_ARREADY),//Write address ready
		.MEM_ARID			(SLAVE4_ARID), // Read address ID //decide on the ID
		.MEM_ARADDR		(SLAVE4_ARADDR), //Read address //decide on the address width
		.MEM_ARVALID	(SLAVE4_ARVALID),//Read address ready
		//read data channel
		.MEM_RID			(SLAVE4_RID),//Read ID tag
		.MEM_RDATA		(SLAVE4_RDATA),//Read data
		.MEM_RVALID		(SLAVE4_RVALID),//Read valid
		.MEM_RREADY		(SLAVE4_RREADY)//Read ready
); 

axi_slave_mem   mem5_inst(
		.ACLK 				(ACLK), //global clock
		.ARESETn 			(ARESETn), //global reset
		//write address channel
		.MEM_AWREADY	(SLAVE5_AWREADY),//Write address ready
		.MEM_AWID			(SLAVE5_AWID), // Write address ID //decide on the ID
		.MEM_AWADDR		(SLAVE5_AWADDR), //Write address //decide on the address width
		.MEM_AWVALID	(SLAVE5_AWVALID),//Write address ready
		//Write data channel
		.MEM_WREADY		(SLAVE5_WREADY),//Write ready
		.MEM_WID			(SLAVE5_WID),//Write ID tag
		.MEM_WDATA		(SLAVE5_WDATA),//Write data
		.MEM_WVALID		(SLAVE5_WVALID),//Write valid
		//write response channel
		.MEM_BID			(SLAVE5_BID),//Response ID tag
		.MEM_BRESP		(SLAVE5_BRESP),//Write response
		.MEM_BVALID		(SLAVE5_BVALID),//Write response valid
		.MEM_BREADY		(SLAVE5_BREADY),//Response ready
		//read address channel
		.MEM_ARREADY	(SLAVE5_ARREADY),//Write address ready
		.MEM_ARID			(SLAVE5_ARID), // Read address ID //decide on the ID
		.MEM_ARADDR		(SLAVE5_ARADDR), //Read address //decide on the address width
		.MEM_ARVALID	(SLAVE5_ARVALID),//Read address ready
		//read data channel
		.MEM_RID			(SLAVE5_RID),//Read ID tag
		.MEM_RDATA		(SLAVE5_RDATA),//Read data
		.MEM_RVALID		(SLAVE5_RVALID),//Read valid
		.MEM_RREADY		(SLAVE5_RREADY)//Read ready
); 

axi_slave_mem   mem6_inst(
		.ACLK 				(ACLK), //global clock
		.ARESETn 			(ARESETn), //global reset
		//write address channel
		.MEM_AWREADY	(SLAVE6_AWREADY),//Write address ready
		.MEM_AWID			(SLAVE6_AWID), // Write address ID //decide on the ID
		.MEM_AWADDR		(SLAVE6_AWADDR), //Write address //decide on the address width
		.MEM_AWVALID	(SLAVE6_AWVALID),//Write address ready
		//Write data channel
		.MEM_WREADY		(SLAVE6_WREADY),//Write ready
		.MEM_WID			(SLAVE6_WID),//Write ID tag
		.MEM_WDATA		(SLAVE6_WDATA),//Write data
		.MEM_WVALID		(SLAVE6_WVALID),//Write valid
		//write response channel
		.MEM_BID			(SLAVE6_BID),//Response ID tag
		.MEM_BRESP		(SLAVE6_BRESP),//Write response
		.MEM_BVALID		(SLAVE6_BVALID),//Write response valid
		.MEM_BREADY		(SLAVE6_BREADY),//Response ready
		//read address channel
		.MEM_ARREADY	(SLAVE6_ARREADY),//Write address ready
		.MEM_ARID			(SLAVE6_ARID), // Read address ID //decide on the ID
		.MEM_ARADDR		(SLAVE6_ARADDR), //Read address //decide on the address width
		.MEM_ARVALID	(SLAVE6_ARVALID),//Read address ready
		//read data channel
		.MEM_RID			(SLAVE6_RID),//Read ID tag
		.MEM_RDATA		(SLAVE6_RDATA),//Read data
		.MEM_RVALID		(SLAVE6_RVALID),//Read valid
		.MEM_RREADY		(SLAVE6_RREADY)//Read ready
);

axi_slave_mem   mem7_inst(
		.ACLK 				(ACLK), //global clock
		.ARESETn 			(ARESETn), //global reset
		//write address channel
		.MEM_AWREADY	(SLAVE7_AWREADY),//Write address ready
		.MEM_AWID			(SLAVE7_AWID), // Write address ID //decide on the ID
		.MEM_AWADDR		(SLAVE7_AWADDR), //Write address //decide on the address width
		.MEM_AWVALID	(SLAVE7_AWVALID),//Write address ready
		//Write data channel
		.MEM_WREADY		(SLAVE7_WREADY),//Write ready
		.MEM_WID			(SLAVE7_WID),//Write ID tag
		.MEM_WDATA		(SLAVE7_WDATA),//Write data
		.MEM_WVALID		(SLAVE7_WVALID),//Write valid
		//write response channel
		.MEM_BID			(SLAVE7_BID),//Response ID tag
		.MEM_BRESP		(SLAVE7_BRESP),//Write response
		.MEM_BVALID		(SLAVE7_BVALID),//Write response valid
		.MEM_BREADY		(SLAVE7_BREADY),//Response ready
		//read address channel
		.MEM_ARREADY	(SLAVE7_ARREADY),//Write address ready
		.MEM_ARID			(SLAVE7_ARID), // Read address ID //decide on the ID
		.MEM_ARADDR		(SLAVE7_ARADDR), //Read address //decide on the address width
		.MEM_ARVALID	(SLAVE7_ARVALID),//Read address ready
		//read data channel
		.MEM_RID			(SLAVE7_RID),//Read ID tag
		.MEM_RDATA		(SLAVE7_RDATA),//Read data
		.MEM_RVALID		(SLAVE7_RVALID),//Read valid
		.MEM_RREADY		(SLAVE7_RREADY)//Read ready
);
	
axi_interconnect axi_intercon_inst (
		 .ACLK				(ACLK),
		 .ARESETn			(ARESETn),  

         //////////////////////////////////////////////////////////////////////////
         //Processor0 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
		 .PROC0_AWREADY			(PROC0_AWREADY),//Write address ready
		 .PROC0_AWID				(PROC0_AWID), // Write address ID //decide on the ID
		 .PROC0_AWADDR			(PROC0_AWADDR), //Write address //decide on the address width
		 .PROC0_AWVALID			(PROC0_AWVALID),//Write address ready
		//Write data channel
		 .PROC0_WREADY			(PROC0_WREADY),//Write ready
		 .PROC0_WID					(PROC0_WID),//Write ID tag
		 .PROC0_WDATA				(PROC0_WDATA),//Write data
		 .PROC0_WVALID			(PROC0_WVALID),//Write valid
		//write response channel
		 .PROC0_BID					(PROC0_BID),//Response ID tag
		 .PROC0_BRESP				(PROC0_BRESP),//Write response
		 .PROC0_BVALID			(PROC0_BVALID),//Write response valid
		 .PROC0_BREADY			(PROC0_BREADY),//Response ready
		//read address channel
		 .PROC0_ARREADY			(PROC0_ARREADY),//Write address ready
		 .PROC0_ARID				(PROC0_ARID), // Read address ID //decide on the ID
		 .PROC0_ARADDR			(PROC0_ARADDR), //Read address //decide on the address width
		 .PROC0_ARVALID			(PROC0_ARVALID),//Read address ready
		//read data channel
		 .PROC0_RID					(PROC0_RID),//Read ID tag
		 .PROC0_RDATA				(PROC0_RDATA),//Read data
		 .PROC0_RVALID			(PROC0_RVALID),//Read valid
		 .PROC0_RREADY			(PROC0_RREADY),//Read ready

         //////////////////////////////////////////////////////////////////////////
         //Processor1 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
		 .PROC1_AWREADY			(PROC1_AWREADY),//Write address ready
		 .PROC1_AWID				(PROC1_AWID), // Write address ID //decide on the ID
		 .PROC1_AWADDR			(PROC1_AWADDR), //Write address //decide on the address width
		 .PROC1_AWVALID			(PROC1_AWVALID),//Write address ready
		//Write data channel
		 .PROC1_WREADY			(PROC1_WREADY),//Write ready
		 .PROC1_WID					(PROC1_WID),//Write ID tag
		 .PROC1_WDATA				(PROC1_WDATA),//Write data
		 .PROC1_WVALID			(PROC1_WVALID),//Write valid
		//write response channel
		 .PROC1_BID					(PROC1_BID),//Response ID tag
		 .PROC1_BRESP				(PROC1_BRESP),//Write response
		 .PROC1_BVALID			(PROC1_BVALID),//Write response valid
		 .PROC1_BREADY			(PROC1_BREADY),//Response ready
		//read address channel
		 .PROC1_ARREADY			(PROC1_ARREADY),//Write address ready
		 .PROC1_ARID				(PROC1_ARID), // Read address ID //decide on the ID
		 .PROC1_ARADDR			(PROC1_ARADDR), //Read address //decide on the address width
		 .PROC1_ARVALID			(PROC1_ARVALID),//Read address ready
		//read data channel
		 .PROC1_RID					(PROC1_RID),//Read ID tag
		 .PROC1_RDATA				(PROC1_RDATA),//Read data
		 .PROC1_RVALID			(PROC1_RVALID),//Read valid
		 .PROC1_RREADY			(PROC1_RREADY),//Read ready		 
		 
         //////////////////////////////////////////////////////////////////////////
         //Processor2 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
		 .PROC2_AWREADY			(PROC2_AWREADY),//Write address ready
		 .PROC2_AWID				(PROC2_AWID), // Write address ID //decide on the ID
		 .PROC2_AWADDR			(PROC2_AWADDR), //Write address //decide on the address width
		 .PROC2_AWVALID			(PROC2_AWVALID),//Write address ready
		//Write data channel
		 .PROC2_WREADY			(PROC2_WREADY),//Write ready
		 .PROC2_WID					(PROC2_WID),//Write ID tag
		 .PROC2_WDATA				(PROC2_WDATA),//Write data
		 .PROC2_WVALID			(PROC2_WVALID),//Write valid
		//write response channel
		 .PROC2_BID					(PROC2_BID),//Response ID tag
		 .PROC2_BRESP				(PROC2_BRESP),//Write response
		 .PROC2_BVALID			(PROC2_BVALID),//Write response valid
		 .PROC2_BREADY			(PROC2_BREADY),//Response ready
		//read address channel
		 .PROC2_ARREADY			(PROC2_ARREADY),//Write address ready
		 .PROC2_ARID				(PROC2_ARID), // Read address ID //decide on the ID
		 .PROC2_ARADDR			(PROC2_ARADDR), //Read address //decide on the address width
		 .PROC2_ARVALID			(PROC2_ARVALID),//Read address ready
		//read data channel
		 .PROC2_RID					(PROC2_RID),//Read ID tag
		 .PROC2_RDATA				(PROC2_RDATA),//Read data
		 .PROC2_RVALID			(PROC2_RVALID),//Read valid
		 .PROC2_RREADY			(PROC2_RREADY),//Read ready
		 
         //////////////////////////////////////////////////////////////////////////
         //Processor3 Interface////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
		 .PROC3_AWREADY			(PROC3_AWREADY),//Write address ready
		 .PROC3_AWID				(PROC3_AWID), // Write address ID //decide on the ID
		 .PROC3_AWADDR			(PROC3_AWADDR), //Write address //decide on the address width
		 .PROC3_AWVALID			(PROC3_AWVALID),//Write address ready
		//Write data channel
		 .PROC3_WREADY			(PROC3_WREADY),//Write ready
		 .PROC3_WID					(PROC3_WID),//Write ID tag
		 .PROC3_WDATA				(PROC3_WDATA),//Write data
		 .PROC3_WVALID			(PROC3_WVALID),//Write valid
		//write response channel
		 .PROC3_BID					(PROC3_BID),//Response ID tag
		 .PROC3_BRESP				(PROC3_BRESP),//Write response
		 .PROC3_BVALID			(PROC3_BVALID),//Write response valid
		 .PROC3_BREADY			(PROC3_BREADY),//Response ready
		//read address channel
		 .PROC3_ARREADY			(PROC3_ARREADY),//Write address ready
		 .PROC3_ARID				(PROC3_ARID), // Read address ID //decide on the ID
		 .PROC3_ARADDR			(PROC3_ARADDR), //Read address //decide on the address width
		 .PROC3_ARVALID			(PROC3_ARVALID),//Read address ready
		//read data channel
		 .PROC3_RID					(PROC3_RID),//Read ID tag
		 .PROC3_RDATA				(PROC3_RDATA),//Read data
		 .PROC3_RVALID			(PROC3_RVALID),//Read valid
		 .PROC3_RREADY			(PROC3_RREADY),//Read ready


				//////////////////////////////////////////////////////////////////////////
        //SLAVE4 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		.SLAVE4_AWREADY			(SLAVE4_AWREADY),//Write address ready
		.SLAVE4_AWID				(SLAVE4_AWID), // Write address ID //decide on the ID
		.SLAVE4_AWADDR			(SLAVE4_AWADDR), //Write address //decide on the address width
		.SLAVE4_AWVALID			(SLAVE4_AWVALID),//Write address ready
		//Write data channel
		.SLAVE4_WREADY			(SLAVE4_WREADY),//Write ready
		.SLAVE4_WID					(SLAVE4_WID),//Write ID tag
		.SLAVE4_WDATA				(SLAVE4_WDATA),//Write data
		.SLAVE4_WVALID			(SLAVE4_WVALID),//Write valid
		//write response channel
		.SLAVE4_BID					(SLAVE4_BID),//Response ID tag
		.SLAVE4_BRESP				(SLAVE4_BRESP),//Write response
		.SLAVE4_BVALID			(SLAVE4_BVALID),//Write response valid
		.SLAVE4_BREADY			(SLAVE4_BREADY),//Response ready
		//read address channel
		.SLAVE4_ARREADY			(SLAVE4_ARREADY),//Write address ready
		.SLAVE4_ARID				(SLAVE4_ARID), // Read address ID //decide on the ID
		.SLAVE4_ARADDR			(SLAVE4_ARADDR), //Read address //decide on the address width
		.SLAVE4_ARVALID			(SLAVE4_ARVALID),//Read address ready
		//read data channel
		.SLAVE4_RID					(SLAVE4_RID),//Read ID tag
		.SLAVE4_RDATA				(SLAVE4_RDATA),//Read data
		.SLAVE4_RVALID			(SLAVE4_RVALID),//Read valid
		.SLAVE4_RREADY			(SLAVE4_RREADY),//Read ready		 
		 
        //////////////////////////////////////////////////////////////////////////
        //SLAVE5 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		.SLAVE5_AWREADY			(SLAVE5_AWREADY),//Write address ready
		.SLAVE5_AWID				(SLAVE5_AWID), // Write address ID //decide on the ID
		.SLAVE5_AWADDR			(SLAVE5_AWADDR), //Write address //decide on the address width
		.SLAVE5_AWVALID			(SLAVE5_AWVALID),//Write address ready
		//Write data channel
		.SLAVE5_WREADY			(SLAVE5_WREADY),//Write ready
		.SLAVE5_WID					(SLAVE5_WID),//Write ID tag
		.SLAVE5_WDATA				(SLAVE5_WDATA),//Write data
		.SLAVE5_WVALID			(SLAVE5_WVALID),//Write valid
		//write response channel
		.SLAVE5_BID					(SLAVE5_BID),//Response ID tag
		.SLAVE5_BRESP				(SLAVE5_BRESP),//Write response
		.SLAVE5_BVALID			(SLAVE5_BVALID),//Write response valid
		.SLAVE5_BREADY			(SLAVE5_BREADY),//Response ready
		//read address channel
		.SLAVE5_ARREADY			(SLAVE5_ARREADY),//Write address ready
		.SLAVE5_ARID				(SLAVE5_ARID), // Read address ID //decide on the ID
		.SLAVE5_ARADDR			(SLAVE5_ARADDR), //Read address //decide on the address width
		.SLAVE5_ARVALID			(SLAVE5_ARVALID),//Read address ready
		//read data channel
		.SLAVE5_RID					(SLAVE5_RID),//Read ID tag
		.SLAVE5_RDATA				(SLAVE5_RDATA),//Read data
		.SLAVE5_RVALID			(SLAVE5_RVALID),//Read valid
		.SLAVE5_RREADY			(SLAVE5_RREADY),//Read ready
		
        //////////////////////////////////////////////////////////////////////////
        //SLAVE6 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		.SLAVE6_AWREADY			(SLAVE6_AWREADY),//Write address ready
		.SLAVE6_AWID				(SLAVE6_AWID), // Write address ID //decide on the ID
		.SLAVE6_AWADDR			(SLAVE6_AWADDR), //Write address //decide on the address width
		.SLAVE6_AWVALID			(SLAVE6_AWVALID),//Write address ready
		//Write data channel
		.SLAVE6_WREADY			(SLAVE6_WREADY),//Write ready
		.SLAVE6_WID					(SLAVE6_WID),//Write ID tag
		.SLAVE6_WDATA				(SLAVE6_WDATA),//Write data
		.SLAVE6_WVALID			(SLAVE6_WVALID),//Write valid
		//write response channel
		.SLAVE6_BID					(SLAVE6_BID),//Response ID tag
		.SLAVE6_BRESP				(SLAVE6_BRESP),//Write response
		.SLAVE6_BVALID			(SLAVE6_BVALID),//Write response valid
		.SLAVE6_BREADY			(SLAVE6_BREADY),//Response ready
		//read address channel
		.SLAVE6_ARREADY			(SLAVE6_ARREADY),//Write address ready
		.SLAVE6_ARID				(SLAVE6_ARID), // Read address ID //decide on the ID
		.SLAVE6_ARADDR			(SLAVE6_ARADDR), //Read address //decide on the address width
		.SLAVE6_ARVALID			(SLAVE6_ARVALID),//Read address ready
		//read data channel
		.SLAVE6_RID					(SLAVE6_RID),//Read ID tag
		.SLAVE6_RDATA				(SLAVE6_RDATA),//Read data
		.SLAVE6_RVALID			(SLAVE6_RVALID),//Read valid
		.SLAVE6_RREADY			(SLAVE6_RREADY),//Read ready

				//////////////////////////////////////////////////////////////////////////
        //SLAVE7 Interface////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
		//write address channel
		.SLAVE7_AWREADY			(SLAVE7_AWREADY),//Write address ready
		.SLAVE7_AWID				(SLAVE7_AWID), // Write address ID //decide on the ID
		.SLAVE7_AWADDR			(SLAVE7_AWADDR), //Write address //decide on the address width
		.SLAVE7_AWVALID			(SLAVE7_AWVALID),//Write address ready
		//Write data channel
		.SLAVE7_WREADY			(SLAVE7_WREADY),//Write ready
		.SLAVE7_WID					(SLAVE7_WID),//Write ID tag
		.SLAVE7_WDATA				(SLAVE7_WDATA),//Write data
		.SLAVE7_WVALID			(SLAVE7_WVALID),//Write valid
		//write response channel
		.SLAVE7_BID					(SLAVE7_BID),//Response ID tag
		.SLAVE7_BRESP				(SLAVE7_BRESP),//Write response
		.SLAVE7_BVALID			(SLAVE7_BVALID),//Write response valid
		.SLAVE7_BREADY			(SLAVE7_BREADY),//Response ready
		//read address channel
		.SLAVE7_ARREADY			(SLAVE7_ARREADY),//Write address ready
		.SLAVE7_ARID				(SLAVE7_ARID), // Read address ID //decide on the ID
		.SLAVE7_ARADDR			(SLAVE7_ARADDR), //Read address //decide on the address width
		.SLAVE7_ARVALID			(SLAVE7_ARVALID),//Read address ready
		//read data channel
		.SLAVE7_RID					(SLAVE7_RID),//Read ID tag
		.SLAVE7_RDATA				(SLAVE7_RDATA),//Read data
		.SLAVE7_RVALID			(SLAVE7_RVALID),//Read valid
		.SLAVE7_RREADY			(SLAVE7_RREADY)//Read ready		

         
	);

endmodule
	
	
	
	
	
	
	
	
	
	
