set files {
						proc_read0 
						proc_read1
						proc_read2
						proc_read3 
						output_memory4
						output_memory5
						output_memory6
						output_memory7
						BRESP_addr0
						BRESP_addr1
						BRESP_addr2
						BRESP_addr3
					}

set flag 0
foreach f1 $files {
	
	
	set file1 [open ./results/$f1.dump]
	set file2 [open ./golden/golden_$f1.dump]
	
	set d1 [read $file1]
	set d2 [read $file2]
	set diff [string compare -nocase $d1 $d2]
	if {$diff != 0} {
		puts "Error in file $f1"
		set flag 1
	}
	close $file1
	close $file2
}

if {$flag != 1} {
	puts "****************************"
	puts "****** Outputs match! ******"
	puts "****************************"
} else {
	puts "Error in a file! Please check!"
}