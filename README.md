# AXI_Project

Implemented ARM’s Advanced Extensible Interconnect protocol to connect 4 processors with 4 memory units in a 2x4 Mesh topology and used deterministic routing to route packets in the mesh of 8 routers with X-first and Y-second. Designed the master and the slave interfaces and the router logic in Verilog and performed system integration and simulation on Questasim.


